<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\User\LoginRequest;

/**
 * Class AuthController
 * @package App\Http\Controllers\Backend
 */
class AuthController extends BackendController
{

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function getLogin()
    {
		$this->_layout = 'admin.auth';

        return $this->render('views.auth.login');
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postLogin(LoginRequest $request)
    {
        $user = $this->auth->login($request);

        if (!$user) {
            return redirect()->back();
        }

       /*if (!$user->hasAccess('administrator')) {
            return redirect()->back();
        }*/

        return redirect()->route('admin.home');
    }

    /**
     * @return mixed
     */
    public function getLogout()
    {
        $this->auth->logout();

        return redirect()->route('admin.auth.login');
    }
}
