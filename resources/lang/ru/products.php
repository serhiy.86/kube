<?php

return [
    'head'                 => 'Товары',
    'title'                => 'Заглавие',
    'body'                 => 'Тело',
    'slug'                 => 'Слизень',
    'excerpt'              => 'Кроме',
    'image'                => 'Фото продукта',
    'price'                => 'Цена продукта',
    'price_title'          => 'Цена наименование товара',
    'rating'               => 'Рейтинг товара',
    'catalog_categorie_id' => 'Категория',
    'meta_title'           => 'Мета-заголовок',
    'meta_description'     => 'Мета-описание',
    'meta_keywords'        => 'Мета-ключевые слова',
    'status'               => 'Положение дел',
    'author_id'            => 'Автор',
    'speaker_id'           => 'Оратор',
    'catalog_filter_id'    => 'Фильтр каталога',
];
