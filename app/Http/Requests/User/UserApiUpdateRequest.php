<?php

namespace App\Http\Requests\User;

use App\Models\User;
use App\Http\Requests\BaseRequest;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Backend\User
 */
class UserApiUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email'      => 'nullable|email',
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            //'phone'      => 'required'
        ];

        return $rules;
    }

    /**
     * @return array
     */
    /*public function messages()
    {
        return [
            'user_name.regex' => trans('validation.user_name_regex')
        ];
    }*/
}
