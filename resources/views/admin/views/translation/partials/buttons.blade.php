<div class="col-sm-6">
    <a href="{!! route('admin.translation.index', $group) !!}" class="btn btn-flat btn-default">@lang('admin_labels.cancel') </a>
</div>

@if ($user->hasAccess('translation.write'))
    <div class="col-sm-6 text-right">
        {!! Form::button(trans('admin_labels.save'), ['class' => 'btn btn-success btn-flat with-loading']) !!}
    </div>
@endif