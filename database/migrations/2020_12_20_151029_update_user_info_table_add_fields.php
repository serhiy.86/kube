<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserInfoTableAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->boolean('reg_complete')->default(false);
            }
        );

        Schema::table(
            'user_info',
            function (Blueprint $table) {
                $table->string('country')->nullable();
                $table->string('type')->nullable();
                $table->string('investments')->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropColumn('reg_complete');
            }
        );
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropColumn('country');
                $table->dropColumn('type');
                $table->dropColumn('investments');
            }
        );
    }
}
