<?php

namespace App\Http\Requests\Backend\News;

use App\Http\Requests\BaseRequest;

/**
 * Class NewsCreateRequest
 * @package App\Http\Requests\Backend\News
 */
class NewsCreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^.*\.(' . implode('|', config('image.allowed_image_extension',[])) . ')$/';
        $rules = [
            //'image' => ['regex:' . $regex],
            'date'  => 'required',
        ];
        $languageRules = [
            'name'          => 'nullable',
            'short_content' => 'required',
            'content'       => 'required',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale . '.' . $name] = $rule;
            }
        }

        return $rules;
    }
}