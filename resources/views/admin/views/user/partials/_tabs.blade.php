<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active">
            <a aria-expanded="false" href="#personal_data" data-toggle="tab">@lang('labels.tab_personal_data')</a>
        </li>

        <li class="@if ($errors->has('roles')) tab-with-errors @endif">
            <a aria-expanded="false" href="#roles" data-toggle="tab">@lang('labels.tab_roles')</a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="personal_data">
            @include('admin.views.user.tabs.personal_data')
        </div>

        <div class="tab-pane" id="roles">
            @include('admin.views.user.tabs.roles')
        </div>
    </div>
</div>
