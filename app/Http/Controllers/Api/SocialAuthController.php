<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\PasswordChangeFromCodeRequest;
use App\Http\Requests\User\PasswordRemindRequest;

use App\Models\User;
use App\Services\UserService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use App\Jobs\SendPasswordReminderEmail;
use App\Jobs\SendUserVerificationApprovedEmail;
use App\Jobs\SendUserWelcomeEmail;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;

use Socialite;

class SocialAuthController extends ApiController
{

    /**
     * Перенаправлення користувача на OAuth.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }


	/**
     * Отримання інформації про користувача від провайдера. Перевірка, чи користувач вже існує в нашій
     * базі даних, дивлячись на їх provider_id в базі даних.
     * Якщо користувач існує, авторизувати його. Інакше, створити нового користувача та авторизувати його. Після 
     * цього перенаправити до сторінки аутентифікації.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try{

        	$providerUser = Socialite::driver($provider)->stateless()->user();

       		$authUser = $this->findUser($providerUser, $provider);

        } catch (\Exception $e) {

	        $authUser = false;
	        \Log::error('handle provider callback',[$e->getMessage()]);
        }
        if($authUser) {

			if(request()->get('referal_code',false)) {
                $user_service = app(UserService::class);

				$user_service->make_referal($authUser, request()->get('referal_code'));
			}

		    if ($token = $this->auth->fromUser($authUser)) {
		        return $this->auth->respondWithToken($token);
		    }
		}
        return response()->json(['data'=> ['error' => ['message'=> 'Unauthorized'] ] ], 418);
    }

	/**
     * Якщо користувач зареєстувався до цього, використовуючи соціальні мережі, повернути користувача
     * інакше, створити нового користувача.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findUser($user, $provider=NULL)
    {
        $authUser = User::findByProvider($user->id,$provider)->first();

		if( ! $authUser ) {
			$email = $user->getEmail() ?? implode('',[$user->id,'@','sls.local']);
			if( User::where('email','=',$email)->first() ) { return false; }

			$credentials = [
				'first_name'=>$user->getName(),
				'last_name'=>'',
				'email'=>$email,
				'password'=>str_random(32)
			];

			$user_service = app(UserService::class);

			$authUser = $user_service->create($credentials,'users',true); // create and activate

			$authUser->social()->create([
				'provider'=>$provider,
				'provider_id'=>$user->id,
			]);
		}

        return $authUser;
    }


	public function handleToken($provider,Request $request) {
	    try{

			$accessToken = $request->get('access_token');
	
			$providerUser = Socialite::driver($provider)->userFromToken($accessToken);
	
			$authUser = $this->findUser($providerUser, $provider);

        } catch (\Exception $e) {

	        \Log::error('handle token',[$e->getMessage()]);
	        $authUser = false;

        }

        if($authUser) {
			if ($token = $this->auth->fromUser($authUser)) {
		    	return $this->auth->respondWithToken($token);
            }
		}

		return response()->json(['data'=> ['error' => ['message'=> 'Unauthorized'] ] ], 418);
	}

	public function linkToken($provider,Request $request) {
		$accessToken = $request->get('access_token');
		
		$providerUser = Socialite::driver($provider)->userFromToken($accessToken);
		
		if( ! $providerUser) {
		return response()->json(['data'=> ['error' => ['message'=> 'Unauthorized'] ] ], 418);
		}

		$authUser = $this->auth->getUser();

		$user_service = app(UserService::class);

		if( User::findByProvider($providerUser->id,$provider)->where('id','<>',$authUser->id)->first() )
		{
			return response()->json(['data'=> 
				[
				'error' => 'Permited',
				'message'=> trans('site_labels.already linked'),
				'status'=>false
				]
			],422);
		}
		$authUser->social()->create([
			'provider'=>$provider,
			'provider_id'=>$providerUser->id,
		]);
		
		return response()->json(['data'=>['error' => '','status'=>true]]);
	}

	public function unlinkToken($provider,Request $request) {

		$authUser = $this->auth->getUser();

		$authUser->social()->where('provider','=',$provider)->delete();
		
		return response()->json(['data'=>['error' => '','status'=>true]]);
	}
}
