<?php

return [
    'head'                 => 'Продукти',
    'title'                => 'Назва',
    'body'                 => 'Тіло',
    'slug'                 => 'Слизень',
    'excerpt'              => 'Крім',
    'image'                => 'Фотопродукт',
    'price'                => 'Ціна товару',
    'price_title'          => 'Назва продукту',
    'rating'               => 'Рейтинг продукту',
    'catalog_categorie_id' => 'Категорія',
    'meta_title'           => 'Метазаголовок',
    'meta_description'     => 'Мета Опис',
    'meta_keywords'        => 'Мета ключові слова',
    'status'               => 'Статус',
    'author_id'            => 'Автор',
    'speaker_id'           => 'Спікер',
    'catalog_filter_id'    => 'Фільтр каталогу',
];
