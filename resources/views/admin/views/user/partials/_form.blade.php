@include('admin.views.user.partials.._buttons', ['class' => 'buttons-top'])

<div class="row">
    <div class="col-sm-12">
        @include('admin.views.user.partials.._tabs')
    </div>
</div>

@include('admin.views.user.partials.._buttons')
