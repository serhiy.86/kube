<?php namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Support;
use Illuminate\Support\Facades\Mail;
use App\Mail\SupportMail;

class SupportController extends Controller
{
    public function index()
    {
        return view('support.form');
    }

    public function store(Request $request)
    {
		$item = new Support();
		$item->fill($request->only(['email','message']));
		$item->save();

		Mail::send(new SupportMail($item->email,$item->message));

        return redirect()->route('home');
    }
}
