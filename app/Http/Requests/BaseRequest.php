<?php

namespace App\Http\Requests;

//use FlashMessages;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BaseRequest
 * @package App\Http\Requests
 */
abstract class BaseRequest extends FormRequest
{

    /**
     * @var array
     */
    protected $defaultRules;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * BaseRequest constructor.
     *
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null  $content
     */
    public function __construct(
        array $query = array (),
        array $request = array (),
        array $attributes = array (),
        array $cookies = array (),
        array $files = array (),
        array $server = array (),
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->defaultRules = [
            'email'                 => ['required', 'email'],
            'password'              => ['required', 'string', 'min:'.config('auth.passwords.min_length')],
            'password_confirmation' => ['required', 'string', 'min:'.config('auth.passwords.min_length')],
            'phone'                 => ['phone'],
            'image'                 => ['regex:'.$this->getImageRegexp()],
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->getMessageBag()->getMessages();

        foreach ($errors as $key => $error) {
            foreach ($error as $_key => $e) {
                preg_match_all(
                    '/(?:^|\s)([a-zA-Z-]{1,}\.[a-zA-Z0-9\.-]{1,}|[a-zA-Z0-9-]{1,}\.[a-zA-Z\.-]{1,})(?:$|\s)/iUs',
                    $e,
                    $matches
                );

                if (!empty($matches[1])) {
                    foreach ($matches[1] as $match) {
                        trim($match, '.');

                        $title = explode('.', $match);
                        $title = trans('validation.attributes.'.array_pop($title));

                        $errors[$key][$_key] = str_replace($match, $title, $e);
                    }

                    if (!$this->ajax() && !$this->wantsJson()) {
//                        FlashMessages::add("error", $errors[$key][$_key]);
                    }

                    continue;
                }

                if (!$this->ajax() && !$this->wantsJson()) {
//                    FlashMessages::add("error", $errors[$key][$_key]);
                }
            }
        }

        parent::failedValidation($validator);
    }

    /**
     * set image regexp based on current env
     */
    private function getImageRegexp()
    {
        return env('APP_ENV') != 'production' ?
            '/.+/' :
            '/^.*\.('.implode('|', config('image.allowed_image_extension')).')$/';
    }
}
