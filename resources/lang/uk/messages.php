<?php

return array(
    'access_denied'                          => 'Доступ заборонено',
    'user register error'                    => 'Помилка реєстрації, спробуйте, будь ласка, пізніше.',
    'user register success message'          => 'Вітаємо! Ви успішно зареєструвалися. На Ваш email надіслано листа для підтвердження активації профілю',
    'user with such email already activated' => 'Аккаунт з даними email вже активований',
    'user with such email was banned'        => 'Аккаунт з даними email забанили',
    'user with such email was blocked'       => 'Аккаунт з даними email заблоковано',
    'user with such email was not activated' => 'Аккаунт з даними email ще не активовано',
    'user with such email was not found'     => 'Аккаунт з даними email не найден',
    'validation_failed'                      => 'Помилка валідації',
    'you already logged in'                  => 'Ви вже залягання',
);
