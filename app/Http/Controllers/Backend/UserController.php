<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\User\PasswordChange;
use App\Models\User;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Controllers\Traits\SaveImageTrait;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
//use App\Jobs\SendUserVerificationApprovedEmail;
use App\Models\UserInfo;
use App\Models\UserSetting;
use App\Services\UserService;
use Carbon\Carbon;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Cartalyst\Sentinel\Users\UserInterface;
use DataTables;
use Exception;
//use FlashMessages;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
//use Meta;
use Redirect;
use Sentinel;
use DB;

/**
 * Class UserController
 * @package App\Http\Controllers\Backend
 */
class UserController extends BackendController
{

    use SaveImageTrait;
    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "user";

    /**
     * @var array
     */
    public $accessMap = [
        'index'                => 'user.read',
        'create'               => 'user.create',
        'store'                => 'user.create',
        'show'                 => 'user.read',
        'edit'                 => 'user.read',
        'update'               => 'user.write',
        'destroy'              => 'user.delete',
        'getNewPassword'       => 'user.write',
        'postNewPassword'      => 'user.write',
        'localAjaxFieldChange' => 'user.write',
        'ajaxFieldChange'      => 'user.write',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = User::with(['info','setting'])->JoinInfo()->select(
                [
                    'users.id as id',
                    'user_info.first_name as first_name',
                    'user_info.last_name as last_name',
                    'users.email as email',
                    'user_info.phone as phone',
                ]
            );
            return $this->_datatable($list);
        }

        $this->data('page_title', trans('labels.users'));
        $this->breadcrumbs(trans('labels.users_list'));

        return $this->render('admin.views.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new User());

        $this->fillAdditionalTemplateData(__FUNCTION__);

        $this->data('page_title', trans('labels.user_create'));

        $this->breadcrumbs(trans('labels.user_create'));

        return $this->render('views.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Backend\User\UserCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\ImageSaveException
     */
    public function store(UserCreateRequest $request)
    {
//        if (!$this->validateImage('avatar')) {
////            FlashMessages::add('warning', trans('messages.bad image'));
//
//            return Redirect::back();
//        }

        try {
            DB::beginTransaction();
        $user = Sentinel::register($request->validated());
        $user_info = $request->all();

        $user->save();

        if ($request->input('activated', false)) {
            $this->userService->activate($user);
        }


        $this->_processInfo($user, $user_info);

        $this->_processSetting($user, $request->get('setting', []));

        $this->_processRoles($user, $request->input('roles', []));

        DB::commit();

//            FlashMessages::add('success', trans('messages.save_ok'));
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add('success', trans('messages.save_failed'));
        }

        return redirect()->route('admin.user.edit', $user->getUserId());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     *
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        /**
         * @var User $model
         */
        $model = User::with(['info','setting'])->whereId($id)->firstOrFail();

        if (!$model) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.user.index');
        }

        $this->data('page_title', $model->getFullName());

        $this->breadcrumbs(trans('labels.user_edit'));

        $this->fillAdditionalTemplateData(__FUNCTION__, $model);

        $this->data('model', $model);

        return $this->render('views.user.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int                                              $id
     * @param \App\Http\Requests\Backend\User\UserUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \App\Exceptions\ImageSaveException
     */
    public function update($id, UserUpdateRequest $request)
    {
        $user = User::with(['info','setting'])->whereId($id)->firstOrFail();

        if (!$user) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.user.index');
        }

//        if (!$this->validateImage('avatar')) {
////            FlashMessages::add('warning', trans('messages.bad image'));
//
//            return Redirect::back();
//        }

        $data = $request->validated();
        $user->fill($data);
        $user->save();
        $user_info = $request->all();

        if ($request->input('activated', false)) {
            $this->userService->activate($user);
        } else {
            $this->userService->deactivate($user);
        }

        $this->_processInfo($user, $user_info);
        $this->_processSetting($user, $request->get('setting', []));

        $this->_processRoles($user, $request->get('roles', []));

//        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.user.edit', $user->getUserId());
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function getNewPassword($id)
    {
        $model = Sentinel::getUserRepository()->findById($id);

        if (!$model) {
//            FlashMessages::add('error', trans("messages.record_not_found"));

            return redirect()->route('admin.user.index');
        }

        $this->data('model', $model);

        $this->data('page_title', trans('labels.password_edit'));

        return $this->render('views.user.new_password');
    }

    /**
     * @param                       $id
     * @param PasswordChange $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postNewPassword($id, PasswordChange $request)
    {
        $user = Sentinel::getUserRepository()->findById($id);

        Sentinel::update($user, ['password' => $request->input('password')]);

        Sentinel::logout($user, true);

//        FlashMessages::add("success", trans("messages.save_ok"));

        return redirect()->route('admin.user.edit', $id);
    }

    /**
     * change field = $field of record with id = $id
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function localAjaxFieldChange($id)
    {
        $field = request('field', null);

        if ($field != 'activated') {
            return $this->ajaxFieldChange($id);
        }

        $model = Sentinel::getUserRepository()->findById($id);

        if ($model) {
            if (request('value', false)) {
                $this->userService->activate($model);
            } else {
                $this->userService->deactivate($model);
            }

            $event = '\App\Events\Backend\UserEdit';

            if (class_exists($event)) {
                event(new $event($model));
            }

            return response()->json(
                [
                    "error"   => 0,
                    'message' => trans('messages.field_value_successfully_saved'),
                    'type'    => 'success',
                ]
            );
        }

        return response()->json(
            [
                "error"   => 1,
                'message' => trans('messages.error_in_field_value_saving'),
                'type'    => 'error',
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // Find the user using the user id
        $user = User::find($id);

        if (!$user) {
//            FlashMessages::add('error', trans("User was not found."));
            return redirect()->back()->withInput();
        }

        // Delete the user
        if ($id <> 1) {
            $user->delete();
        }

        return Redirect::route('admin.user.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        try {
            $search_text = $request->get('search_text', '');

            if ($search_text) {
                $users = User::with(['info','setting'])->JoinInfo()->where('user_info.first_name', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('user_info.last_name', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('users.email', 'like', '%'.$request->get('text', '').'%')
                    ->orWhere('user_info.phone', 'like', '%'.$request->get('text', '').'%');
            } else {
                $users = User::with(['info','setting'])->JoinInfo()->whereNotNull('users.email');
            }

            $users = $users->select('users.id', 'user_name', 'users.email', 'user_info.phone', 'user_info.first_name', 'user_info.last_name')->paginate(15);

            $items = [];

            /**
             * @var User $user
             */
            foreach ($users as $user) {
                $full_name = $user->getFullName();

                $info = '('.$user->email.($user->info->phone ? ', '.$user->info->phone : '').')';

                $items[] = [
                    'id'        => $user->id,
                    'full_name' => '# '.$user->id.' '.trim($full_name.' '.$info),
                ];
            }

            return [
                'status' => 'success',
                'items'  => $items,
            ];
        } catch (Exception $e) {
            return [
                'status' => 'error',
                'items'  => [],
            ];
        }
    }

    /**
     * @param           $function
     * @param User|null $model
     */
    public function fillAdditionalTemplateData($function, $model = null)
    {
        switch ($function) {
            case 'create' :
                $this->data('user_roles', []);
                break;
            case 'edit' :
                $this->data('user_roles', $model->roles->pluck('id')->toArray());
                break;
        }

        $roles = [];
        foreach (EloquentRole::all() as $item) {
            $roles[$item['id']] = $item['name'];
        }
        $this->data('roles', $roles);
    }

    /**
     * @param \Cartalyst\Sentinel\Users\UserInterface $user
     * @param array                                   $roles
     */
    private function _processRoles(UserInterface $user, $roles = [])
    {
        if ($this->user->hasAccess('user.role.write')) {
            $user->roles()->sync($roles);
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $list
     *
     * @return mixed
     * @throws \Exception
     */
    private function _datatable(Builder $list)
    {
        return $dataTables = DataTables::of($list)
            ->filterColumn(
                'actions',
                function ($query, $keyword) {
                    $query->whereRaw('users.id like ?', ['%'.$keyword.'%']);
                }
            )
            ->filterColumn(
                'full_name',
                function ($query, $keyword) {
                    $query->whereRaw(
                        'user_info.first_name like ? or user_info.last_name like ?',
                        ['%'.$keyword.'%', '%'.$keyword.'%']
                    );
                }
            )
            ->filterColumn(
                'phone',
                function ($query, $keyword) {
                    $query->whereRaw('user_info.phone like ?', ['%'.$keyword.'%']);
                }
            )
            ->addColumn(
                'full_name',
                function ($model) {
                    return $model->getFullName();
                }
            )
            ->addColumn(
                'actions',
                function ($model) {
                    return view(
                        'views.'.$this->module.'.partials.control_buttons',
                        ['model' => $model, 'type' => 'user', 'without_delete' => false]
                    )->render();
                }
            )
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @param \App\Models\User $user
     * @param array            $user_info
     *
     * @throws \App\Exceptions\ImageSaveException
     */
    private function _processInfo(User $user, $user_info = [])
    {
        $user_info['id'] = $user->id;
        $this->setImage($user_info, 'avatar', $this->module);

        $info = $user->info()->first();
        if (empty($info)) {
            $info = new UserInfo();
            $info->fill($user_info);
            $user->info()->save($info);
        } else {
            $info->update($user_info);
        }
    }

    /**
     * @param \App\Models\User $user
     * @param array            $user_setting
     *
     */
    private function _processSetting(User $user, $user_setting = [])
    {
        $user_setting['id'] = $user->id;

        $setting = $user->setting()->first();
        if (empty($setting)) {
//            $setting = new UserSetting();
//            $setting->fill(['locale' => config('app.locale')]);
            $user->setting()->create(['locale' => config('app.locale')]);
        } else {
            $setting->update($user_setting);
        }
    }
}
