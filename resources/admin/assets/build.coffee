###
Base imports and vars
###
path = require 'path'
gulp = require 'gulp'
sass = require 'gulp-sass'
prefix = require 'gulp-autoprefixer'
util = require 'gulp-util'
concat = require 'gulp-concat'
uglify = require 'gulp-uglify'
coffee = require 'gulp-coffee'
imagemin = require 'gulp-imagemin'
cleanCSS = require 'gulp-clean-css'
plumber = require 'gulp-plumber'


# get the theme name
theme = 'admin'

projectRoot = __dirname.slice(0, __dirname.indexOf(path.sep + 'resources' + path.sep))

console.log(projectRoot);

dev_path =
  fonts: __dirname.concat('/fonts/**')
  vendor: __dirname.concat('/vendor/**')
  images: __dirname.concat('/images/**')
  coffee: __dirname.concat('/coffee/**/*.coffee')
  js: __dirname.concat('/js/**/*.js')
  sass: __dirname.concat('/sass/**/*.sass')

prod_path =
  fonts: projectRoot.concat('/public/assets/themes/' + theme + '/fonts/')
  vendor: projectRoot.concat('/public/assets/themes/' + theme + '/vendor/')
  images: projectRoot.concat('/public/assets/themes/' + theme + '/images/')
  js: projectRoot.concat('/public/assets/themes/' + theme + '/js/')
  css: projectRoot.concat('/public/assets/themes/' + theme + '/css/')


# SASS #
_css = ->
  gulp.src(dev_path.sass)
    .pipe(plumber())
    .pipe(sass({style: 'compressed'}))
    .pipe(prefix())
    .pipe(cleanCSS(removeEmpty: true))
    .pipe(concat("styles.css"))
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

_css_dev = ->
  gulp.src(dev_path.sass)
    .pipe(plumber())
    .pipe(sass({style: 'expanded'}))
    .pipe(prefix())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(prod_path.css))
    .on('error', plumber)

_css_watch = ->
  gulp.watch dev_path.sass, {interval: 1000, usePolling: true}, _css_dev()


# COFFEE #
_coffee = ->
  gulp.src(dev_path.coffee)
    .pipe(plumber())
    .pipe concat 'main.js'
    .pipe(coffee({bare: true}))
    .pipe(uglify())
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

_coffee_dev = ->
  gulp.src(dev_path.coffee)
    .pipe(plumber())
    .pipe concat 'main.js'
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

_coffee_watch = ->
  gulp.watch dev_path.coffee, {interval: 1000, usePolling: true}, _coffee_dev()


# PUREJS #
_purejs = ->
  gulp.src(dev_path.js)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(gulp.dest(prod_path.js))
    .on('error', plumber)

_purejs_watch = ->
  gulp.watch dev_path.js, {interval: 1000, usePolling: true}, _purejs()


# IMAGES #
_images = ->
  gulp.src(dev_path.images)
    .pipe(plumber())
    .pipe(imagemin())
    .pipe(gulp.dest(prod_path.images))
    .on('error', plumber)

_images_watch = ->
  gulp.watch dev_path.images, {interval: 1000, usePolling: true}, _images()


# FONTS #
_fonts = ->
  gulp.src(dev_path.fonts)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.fonts))
    .on('error', plumber)

_fonts_watch = ->
  gulp.watch dev_path.fonts, {interval: 1000, usePolling: true}, _fonts()


# VENDOR #
_vendor = ->
  gulp.src(dev_path.vendor)
    .pipe(plumber())
    .pipe(gulp.dest(prod_path.vendor))
    .on('error', plumber)

_vendor_watch = ->
  gulp.watch dev_path.vendor, {interval: 1000, usePolling: true}, _vendor()


_default = ->
  gulp.parallel(
    _css,
    _coffee,
    _purejs,
    _images,
    _fonts,
    _vendor
  )

_dev = ->
  gulp.parallel(
    _css_dev,
    _coffee_dev,
    _purejs,
    _images,
    _fonts,
    _vendor
  )

_watch = ->
  gulp.parallel(
    _css_watch,
    _coffee_watch,
    _purejs_watch,
    _images_watch,
    _fonts_watch,
    _vendor_watch
  )


# Export tasks #
module.exports =
  default: _default()
  dec: _dev()
  watch: _watch()

