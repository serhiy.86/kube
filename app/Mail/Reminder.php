<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Reminder
 * @package App\Mail
 */
class Reminder extends Mailable
{

    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $view = 'emails.user.reminder';

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $code;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User $user
     * @param string             $code
     */
    public function __construct(User $user, string $code)
    {
        $this->email = $user->email;
        $this->code = $code;

        $this->to($user->email, $user->getFullName());
        $this->subject(trans('auth.reminder_email'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with(
            [
                'link' =>  config('links.reset_password') . '?code=' . $this->code . '&email=' . $this->email,
            ]
        );
    }
}
