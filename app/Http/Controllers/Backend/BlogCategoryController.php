<?php

namespace App\Http\Controllers\Backend;

use App\Models\BlogCategory;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\BlogCategory\BlogCategoryCreateRequest;
use App\Http\Requests\Backend\BlogCategory\BlogCategoryUpdateRequest;
use DataTables;
//use FlashMessages;
use Illuminate\Http\Request;
use Response;

/**
 * Class BlogCategoryController
 * @package App\Http\Controllers\Backend
 */
class BlogCategoryController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "blog_category";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'blog_category.read',
        'create'          => 'blog_category.create',
        'store'           => 'blog_category.create',
        'show'            => 'blog_category.read',
        'edit'            => 'blog_category.read',
        'update'          => 'blog_category.write',
        'destroy'         => 'blog_category.delete',
        'ajaxFieldChange' => 'blog_category.write',
    ];

    /**
     * Display a listing of the resource.
     * GET /blog_category
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = BlogCategory::joinTranslations('blog_categories', 'blog_category_translations', 'id', 'blog_category_id')
                ->select(
                    'blog_categories.id as id',
                    'blog_category_translations.title as title',
                    'blog_categories.status as status'
                );

            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'title',
                    function ($query, $keyword) {
                        return $query->whereRaw('blog_category_translations.title like ?', ['%'.$keyword.'%']);
                    }
                )
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('blog_categories.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'position', 'actions'])
                ->make();
        }

        $this->data('page_title', trans('labels.all_blog_categories'));
        $this->breadcrumbs(trans('labels.list'));

        return $this->render('views.blog_category.index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /blog_category/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new BlogCategory());

        $this->data('page_title', trans('labels.creating'));

        $this->breadcrumbs(trans('labels.blog_categories'));

        return $this->render('views.blog_category.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /blog_category
     *
     * @param BlogCategoryCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BlogCategoryCreateRequest $request)
    {
        try{
            $model = new BlogCategory();

            $model->fill($request->all());

            $model->save();

            //        FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.blog_category.index');
        } catch (\Exception $e){

        }



    }

    /**
     * Display the specified resource.
     * GET /blog_category/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /blog_category/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Response
     */
    public function edit($id)
    {
        $model = BlogCategory::find($id);

        if (!$model) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.blog_category.index');
        }

        $this->data('page_title', trans('labels.editing'));

        $this->breadcrumbs(trans('labels.blog_categories'));

        return $this->render('views.blog_category.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     * PUT /blog_category/{id}
     *
     * @param  int            $id
     * @param BlogCategoryCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, BlogCategoryUpdateRequest $request)
    {
        $model = BlogCategory::find($id);

        if (!$model) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.blog_category.index');
        }

        $model->update($request->all());

//        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.blog_category.index');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /blog_category/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = BlogCategory::find($id);

        if (!$model) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.blog_category.index');
        }

        $model->delete();

//        FlashMessages::add('success', trans("messages.destroy_ok"));

        return redirect()->route('admin.blog_category.index');
    }
}
