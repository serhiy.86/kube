<?php

namespace App\Http\Requests\Backend\Faq;

use App\Http\Requests\BaseRequest;

/**
 * Class FaqRequest
 * @package App\Http\Requests\Faq
 */
class FaqRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'faq_category_id' => 'required|exists:faq_category,id',
            'status'        => 'required|boolean',
            'position'      => 'required|integer',
        ];
        
        $languageRules = [
            'question' => 'string|required',
            'answer'   => 'string|required',
        ];
        
        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = sprintf($rule, $locale);
            }
        }
        
        return $rules;
    }
}