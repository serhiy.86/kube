<?php

return array(
    'access_denied'                          => 'Доступ запрещен',
    'user register error'                    => 'Ошибка регистрации, попробуйте, пожалуйста, позже.',
    'user register success message'          => 'Поздравляем! Вы успешно зарегистрировались. На Ваш email отправлено письмо для подтверждения активации профиля',
    'user with such email already activated' => 'Аккаунт с данным email уже активирован',
    'user with such email was banned'        => 'Аккаунт с данным email забанен',
    'user with such email was blocked'       => 'Аккаунт с данным email заблокирован',
    'user with such email was not activated' => 'Аккаунт с данным email еще не активирован',
    'user with such email was not found'     => 'Аккаунт с данным email не найден',
    'validation_failed'                      => 'Ошибка валидации',
    'you already logged in'                  => 'Вы уже залогинены',
);
