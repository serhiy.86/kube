<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\News\NewsCreateRequest;
use App\Http\Requests\Backend\News\NewsUpdateRequest;
use App\Models\News;
use App\Models\Image;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use DataTables;
use DB;
use Exception;
//use FlashMessages;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Meta;
use Redirect;
use Response;
use Yajra\DataTables\Html\Builder;

/**
 * Class NewsController
 * @package App\Http\Controllers\Backend
 */
class NewsController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "news";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'news.read',
        'create'          => 'news.create',
        'store'           => 'news.create',
        'show'            => 'news.read',
        'edit'            => 'news.read',
        'update'          => 'news.write',
        'destroy'         => 'news.delete',
        'ajaxFieldChange' => 'news.write',
    ];

    /**
     * Display a listing of the resource.
     * GET /News
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request, Builder $builder)
    {
        if (request()->ajax()) {
            $list = News::withTranslations()->joinTranslations('news', 'news_translations')->select(
                    'news.id',
                    'news_translations.name',
                    'news.date',
                    'status'
                );
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        $query->whereRaw("news_translations.name like ?", ["%{$keyword}%"]);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'front_link' => false, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'actions'])
                ->make();

        }

        $this->data('page_title', trans('labels.news'));
        $this->breadcrumbs(trans('labels.list'));

$html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => trans('labels.id')],
                ['data' => 'name', 'name' => 'name', 'title' => trans('labels.name')],
                ['data' => 'actions', 'name' => 'actions', 'title' => trans('labels.actions')],
            ])
			->parameters([
				'sPaginationType' => "bootstrap_alt",
				'drawCallback' => 'function() { initToggles(); }',
								/*'language' => ["url" => '//cdn.datatables.net/plug-ins/1.10.20/i18n/'.config('laravellocalization.supportedLocales.'.$this->locale.'.name','Russian').'.json'],*/
			]);

        return $this->render('views.'.$this->module.'.index')->with('html',$html);
    }

    /**
     * Show the form for creating a new resource.
     * GET /news/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new News());

        $this->data('page_title', trans('labels.creating'));

        $this->breadcrumbs(trans('labels.creating'));

        return $this->render('views.'.$this->module.'.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /News
     *
     * @param NewsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(NewsCreateRequest $request)
    {
        DB::beginTransaction();

        try {

            $input = $request->all();
            $model = new News($input);
            $model->status = $input['status'];
            $model->save();

            DB::commit();

            //FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

            //FlashMessages::add('error', trans('messages.save_failed'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * GET /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /news/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        try {
            $model = News::findOrFail($id);

            $this->data('page_title', trans('labels.editing'));

            $this->breadcrumbs(trans('labels.editing'));

            return $this->render('views.'.$this->module.'.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /news/{id}
     *
     * @param  int           $id
     * @param NewsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, NewsUpdateRequest $request)
    {
        try {
            $input = $request->all();
            $model = News::findOrFail($id);

            $model->fill($input);
            $model->status = $input['status'];

            $model->save();

            //FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

            //FlashMessages::add("error", trans('messages.update_error'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /news/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $model = News::findOrFail($id);

            if (!$model->delete()) {
                //FlashMessages::add("error", trans("messages.destroy_error"));
            } else {
                //FlashMessages::add('success', trans("messages.destroy_ok"));
            }
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
            //FlashMessages::add("error", trans('messages.delete_error'));
        }

        return redirect()->route('admin.'.$this->module.'.index');
    }
}
