@extends('layouts.auth')

@section('content')

   <div class="row login-block">
       <div class="col-md-6 col-lg-4 col-md-offset-3 col-lg-offset-4">
           <div class="box box-primary">
               <div class="box-header with-border">
                   <h3 class="box-title">@lang('admin_labels.login_form_heading')</h3>
               </div>

               {!! Form::open(array("id" => "login_form", "role" => "form", 'class' => 'form-horizontal', "route" => 'admin.auth.login.post')) !!}
                   <div class="box-body">
                       <div class="form-group">
                           <label for="email" class="col-sm-2 control-label">@lang('admin_labels.email')</label>

                           <div class="col-sm-10">
                               {!! Form::text('email', '', array("placeholder"=> trans('admin_labels.email'), 'class' => 'form-control input-sm', 'type' => "email" )) !!}
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="password" class="col-sm-2 control-label">@lang('admin_labels.password')</label>

                           <div class="col-sm-10">
                               {!! Form::password('password', array("placeholder"=> trans('admin_labels.password'), 'class' => 'form-control input-sm' )) !!}
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                               <label for="remember" class="checkbox-label">
                                    <input id="remember" name="remember" type="checkbox" class="square" value="1" />

                                    <span class="title">@lang('admin_labels.remember_me')</span>
                               </label>
                           </div>
                       </div>
                   </div>
                   <div class="box-footer">
                       {!! Form::submit(trans('admin_labels.login'), array('class' => 'btn btn-info btn-flat pull-right')) !!}
                   </div>
               {!! Form::close() !!}
           </div>
       </div>
   </div>

@endsection