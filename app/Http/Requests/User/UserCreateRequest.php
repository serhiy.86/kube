<?php

namespace App\Http\Requests\User;

use App\Models\User;
use App\Http\Requests\BaseRequest;

/**
 * Class UserCreateRequest
 * @package App\Http\Requests\Backend\User
 */
class UserCreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'email'                 => 'required|email|unique:users',
//            'phone'                 => 'nullable|phone',
            'first_name'            => 'nullable|string|max:255',
            'last_name'             => 'nullable|string|max:255',
//            'gender'                => 'nullable|integer|in:'.implode(',', array_keys(app(User::class)->getGenders())),
//            'address'               => 'nullable|string|min:2',
//            'city'                  => 'nullable|string|min:2',
//            'country_id'            => 'nullable|integer|exists:countries,id',
//            'roles'                 => 'array',
            'password'              => 'required|confirmed|min:' . config('auth.passwords.min_length'),
        ];

        return $rules;
    }
}
