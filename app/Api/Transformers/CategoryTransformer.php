<?php namespace App\Api\Transformers;

use App\Models\Category;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(Category $item)
    {
        return [
			    'id' => (int)$item->id,
          'slug' => (string)$item->slug,
          'parent_id' => (int)$item->slug,
          'position' => (int)$item->position,
          'name' => (string)$item->name,
          'description' => (string)$item->description,
          'image' => (boolean)$item->image,
          'meta_title' => (string)$item->meta_title,
          'meta_description' => (string)$item->meta_description,
          'meta_keyword' => (string)$item->meta_keyword,
          'guid' => (string)$item->guid,
        ];
    }
}
