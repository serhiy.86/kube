<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;

class HomeController extends FrontendController
{
    public $module="home";


    public function index()
    {
        return $this->view('home');
    }

}
