<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

/**
 * Class PasswordChangeFromCodeRequest
 * @package App\Http\Requests\Backend\User
 */
class PasswordChangeFromCodeRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email|max:255|exists:users',
            'code'     => 'required|string|max:255',
            'password' => 'required|confirmed|min:' . config('auth.passwords.min_length'),
        ];
    }
}
