<div class="row form-group">
    <div class="col-xs-12 col-sm-4 col-md-2">
        <div class="input-group bootstrap-datepicker datepicker">
            {!! Form::text('value', null, ['id' => 'value', 'placeholder' => 'YYYY-MM-DD', 'required' => true, 'class' => 'form-control input-sm datepicker datepicker-birthday inputmask-datepicker']) !!}

            <span class="input-group-addon pointer">
                <i class="datepicker-icon fa fa-calendar" aria-hidden="true"></i>
            </span>
        </div>
    </div>
</div>