<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

/**
 * Class PasswordChangeFromCodeRequest
 * @package App\Http\Requests\Backend\User
 */
class PasswordChange extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'              => 'required|confirmed:password_confirmation|min:'
                .config('auth.passwords.min_length'),
            'password_confirmation' => 'required',
        ];

    }
}
