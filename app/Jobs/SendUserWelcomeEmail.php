<?php

namespace App\Jobs;

use App\Mail\Welcome;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendUserWelcomeEmail
 * @package App\Jobs
 */
class SendUserWelcomeEmail extends BaseJob
{
    
    /**
     * @var \App\Models\User
     */
    private $user;
    
    /**
     * Create a new job instance.
     *
     * @param \App\Models\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new Welcome($this->user));
    }
}
