<?php

return array (

    /*
    |--------------------------------------------------------------------------
    | superuser
    |--------------------------------------------------------------------------
    |
    | Any user with superuser permissions automatically has access to everything,
    | regardless of the user permissions and role permissions.
    |
    */

    'superuser',

    /*
    |--------------------------------------------------------------------------
    | administrator
    |--------------------------------------------------------------------------
    |
    | Разрешает доступ в админку
    |
    */

    'administrator',

    'user.role.write',

    'role.create',
    'role.read',
    'role.write',
    'role.delete',

    'faq.create',
    'faq.read',
    'faq.write',
    'faq.delete',

    'faq_category.create',
    'faq_category.read',
    'faq_category.write',
    'faq_category.delete',

    'variable.value.read',
    'variable.value.write',
    'variable.create',
    'variable.read',
    'variable.write',
    'variable.delete',
);
