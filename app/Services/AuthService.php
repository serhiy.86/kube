<?php

namespace App\Services;

use Illuminate\Http\Request;
use Activation;
use Sentinel;

/**
 * Class AuthService
 * @package App\Http\Controllers\Backend
 */
class AuthService
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function login(Request $request)
    {
        $user = Sentinel::findByCredentials($request->only('email'));

        if (!$user) {
            return false;
        }

        if (!Activation::completed($user)) {
            return false;
        }

        $credentials = $request->only(['email', 'password']);

        if (!Sentinel::validateCredentials($user, $credentials)) {
            return false;
        }

        $user = Sentinel::authenticate($credentials);

        return Sentinel::login($user, $request->get('remember', false));

    }

    /**
     * @return mixed
     */
    public function logout()
    {
        Sentinel::logout();
        return true;
    }

    public function getUser()
    {
        return Sentinel::getUser();
    }
}
