<?php

namespace App\Jobs;

use App\Mail\Reminder;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendUserVerificationApprovedEmail
 * @package App\Jobs
 */
class SendPasswordReminderEmail extends BaseJob
{
    
    /**
     * @var \App\Models\User
     */
    private $user;
    private $code;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\User $user
     * @param string $code
     */
    public function __construct(User $user, string $code)
    {
        $this->user = $user;
        $this->code = $code;
    }
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new Reminder($this->user, $this->code));
    }
}
