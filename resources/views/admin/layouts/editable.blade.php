@extends('admin.layouts.main')

@section('assets.top')
    @parent
    <script src="{!! asset('assets/themes/admin/vendor/adminlte/plugins/ckeditor/ckeditor.js') !!}"></script>
@stop
