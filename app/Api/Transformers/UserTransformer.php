<?php namespace App\Api\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * Resources that can be included if requested.
     *
     * @var array
     */
    protected $availableIncludes = [];

    protected $defaultIncludes = [];

    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     * @return array
     */
    public function transform(User $item)
    {

        return [
            'id'         => (string)$item->id,
            'email'      => (string)$item->email,
            'first_name' => (string)$item->info->first_name,
            'last_name'  => (string)$item->info->last_name,
            'country' 	 => (string)$item->info->country,
            'type' 		 => (string)$item->info->type,
            'investments' => (string)$item->info->investments,
			'avatar'  => (string)$item->info->avatar,

            'registration_complete'  => (bool)$item->reg_complete,
        ];
    }

}
