<?php

namespace App\Providers;

use App\Classes\ImageUploader;
use Illuminate\Support\ServiceProvider;

/**
 * Class ImageUploaderServiceProvider
 * @package App\Providers
 */
class ImageUploaderServiceProvider extends ServiceProvider
{
    
    /**
     * register
     */
    public function register()
    {
        $this->app->bind('image_uploader', ImageUploader::class);
    }
}