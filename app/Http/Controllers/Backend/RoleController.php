<?php

namespace App\Http\Controllers\Backend;

use Module\Role\Contract\Permissions as PermissionsContract;
use App\Http\Requests\Backend\Role\RoleCreateRequest;
use App\Http\Requests\Backend\Role\RoleUpdateRequest;
use Cartalyst\Sentinel\Roles\EloquentRole;
use DataTables;
//use FlashMessages;
use Illuminate\Http\Request;
use Sentinel;

/**
 * Class RoleController
 * @package App\Http\Controllers\Backend
 */
class RoleController extends BackendController
{

    /**
     * @var string
     */
    public $module = "role";

    /**
     * @var array
     */
    public $accessMap = ['all' => 'role'];


    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {

            $list = EloquentRole::select('id', 'name', 'slug');

            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('roles.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'admin.partials.datatables.control_buttons',
                            ['model' => $model, 'type' => 'role']
                        )->render();
                    }
                )
                ->rawColumns(['actions'])
                ->make();
        }

        $this->data('page_title', trans('admin_labels.roles_list'));

        return $this->render('admin.views.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data("model", new EloquentRole());

        $this->data('permissions', $this->permissions->loadArray());

        $this->data('page_title', trans('admin_labels.role_create'));

        $this->_fillAdditionTemplateData();

        return $this->render('views.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Backend\Role\RoleCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleCreateRequest $request)
    {
        $input = $request->only('name', 'slug', 'permissions');

        $this->_fixPermissionArray($input);

        EloquentRole::create($input);

//        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.role.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Response
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $role = Sentinel::findRoleById($id);

        if (!$role) {
//            FlashMessages::add("error", trans('messages.record_not_found'));

            return redirect()->route('admin.role.index');
        }

        $this->data('model', $role);

        $permissions = $this->permissions->loadAndCombine($role->getPermissions());
        $this->data('permissions', $permissions);

        $this->_fillAdditionTemplateData();

        $this->data('page_title', trans('admin_labels.role_edit').' "'.$role->name.'"');

        return $this->render('views.role.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int                                              $id
     * @param \App\Http\Requests\Backend\Role\RoleUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, RoleUpdateRequest $request)
    {
        $role = Sentinel::findRoleById($id);

        if (!$role) {
//            FlashMessages::add("error", trans('messages.record_not_found'));

            return redirect()->route('admin.role.index');
        }

        $input = $request->only('name', 'permissions');

        $this->_fixPermissionArray($input);

        $role->update($input);

//        FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.role.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $role = Sentinel::getRoleRepository()->findById($id);

        if (!$role) {
//            FlashMessages::add("error", trans('messages.record_not_found'));

            return redirect()->route('admin.role.index');
        }

        $role->delete();

//        FlashMessages::add('success', trans('messages.delete_success'));

        return redirect()->route('admin.role.index');
    }

    /**
     * fill additional template data
     */
    private function _fillAdditionTemplateData()
    {
        $this->data('permissions_description', config('permissions_description'));
    }

    /**
     * @param $input
     */
    private function _fixPermissionArray(&$input)
    {
        $permissions = [];

        array_walk(
            $input['permissions'],
            function ($value, $key) use (&$permissions) {
                $permissions[str_replace('_', '.', $key)] = (bool) $value;
            }
        );

        $input['permissions'] = $permissions;
    }
}
