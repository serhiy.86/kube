<?php

namespace App\Http\Controllers\Backend;

use App\Models\Variable;
use App\Http\Requests\Variable\VariableCreateRequest;
use App\Http\Requests\Variable\VariableUpdateRequest;
use App\Http\Requests\Variable\VariableValueUpdateRequest;
use Exception;
//use //FlashMessages;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Meta;
use Yajra\DataTables\Html\Builder;

/**
 * Class VariableController
 * @package App\Http\Controllers\Backend
 */
class VariableController extends BackendController
{
    
    /**
     * @var string
     */
    public $module = "variable";
    
    /**
     * @var array
     */
    public $accessMap = [
        'indexValues' => 'variable.value.read',
        'updateValue' => 'variable.value.write',
        
        'index'   => 'variable.read',
        'create'  => 'variable.create',
        'store'   => 'variable.create',
        'show'    => 'variable.read',
        'edit'    => 'variable.read',
        'update'  => 'variable.write',
        'destroy' => 'variable.delete',
    ];
    
    
    /**
     * Display a listing of the resource.
     * GET /variable
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */
    public function index(Builder $builder)
    {
        return $this->indexValues();
    }
    
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function indexValues()
    {
        $this->data('page_title', trans('labels.variables'));
        $this->breadcrumbs(trans('labels.variables_list'));
        
        $list = Variable::orderBy('is_hidden')->orderBy('name')->get();
        
        $this->data('list', $list);
        
        return $this->render('views.variable.index_values');
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /variable/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new Variable());
        
        $this->data('page_title', trans('labels.variable_create'));
        
        $this->breadcrumbs(trans('labels.variable_create'));
        
        $this->_fillAdditionTemplateData();
        
        return $this->render('views.variable.create');
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /variable
     *
     * @param VariableCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VariableCreateRequest $request)
    {
        $input = $request->only(['type', 'key', 'name', 'description', 'multilingual']);
        
        try {
            Variable::create($input);
            
            //FlashMessages::add('success', trans('messages.save_ok'));
            
            return redirect()->route('admin.variable.index');
        } catch (Exception $e) {
            //FlashMessages::add('error', trans('messages.save_failed'));
            
            return redirect()->back()->withInput();
        }
    }
    
    /**
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     * GET /variable/{id}/edit
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        try {
            $model = Variable::with('translations')->findOrFail($id);
    
            if ($model->is_hidden && !$this->user->hasAccess('superuser')) {
                //FlashMessages::add('error', trans('messages.record_not_found'));
        
                return redirect()->route('admin.variable.index');
            }
            
            $this->data('page_title', '"'.$model->name.'"');
            
            $this->breadcrumbs(trans('labels.variable_editing'));
            
            $this->_fillAdditionTemplateData();
            
            return $this->render('views.variable.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.variable.index');
        }
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /variable/{id}
     *
     * @param int                   $id
     * @param VariableUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, VariableUpdateRequest $request)
    {
        $input = $request->only(['type', 'key', 'name', 'description', 'multilingual']);
        
        try {
            $model = Variable::findOrFail($id);
    
            if ($model->is_hidden && !$this->user->hasAccess('superuser')) {
                //FlashMessages::add('error', trans('messages.record_not_found'));
        
                return redirect()->route('admin.variable.index');
            }
            
            $model->update($input);
            
            //FlashMessages::add('success', trans('messages.save_ok'));
            
            return redirect()->route('admin.variable.index');
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
            //FlashMessages::add("error", trans('messages.update_error').': '.$e->getMessage());
        }
        
        return redirect()->back();
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /variable/{id}
     *
     * @param VariableValueUpdateRequest $request
     *
     * @return array
     */
    public function updateValue(VariableValueUpdateRequest $request)
    {
        try {
            $model = Variable::findOrFail($request->get('variable_id'));
    
            if ($model->is_hidden && !$this->user->hasAccess('superuser')) {
                return [
                    'status'  => 'error',
                    'message' => trans('messages.update_error'),
                ];
            }
            
            $input = $request->except('type', 'key', 'name', 'description', 'multilingual');
            $input['value'] = isset($input['value']) ? $input['value'] : '';

			$model->fill($input);
            
            $model->save();			

			if($model->multilingual) {
				$settings = $request->only(config('translatable.locales'));
				$lang_data = [];
				foreach ($settings as $lang => $fields) {

					$lang_data[$lang] = $model->translate($lang)->toArray();

					if(!is_array($fields)) {

						if(!is_array($lang_data[$lang])) $lang_data[$lang] = $fields;

					} else {

	                foreach ($fields as $key => $value) {
						if(is_array($value)) {
							foreach ($value as $ind=>$json)
							{
							 if(is_object($json)) {
								$uploaded = $json->storeAs('variables/'.$lang.'-'.$model->id, $json->hashName().'.'.$json->getClientOriginalName() , 'uploads');
					            if ($uploaded) {
					                $uploaded = \Storage::disk('uploads')->url($uploaded);
					            }
								$lang_data[$lang][$key][$ind] = $uploaded;
							 } 
							}
						}elseif(is_object($value)) {
							$uploaded = $value->storeAs('variables/'.$lang.'-'.$model->id, $value->hashName().'.'.$value->getClientOriginalName() , 'uploads');
				            if ($uploaded) {
				                $uploaded = \Storage::disk('uploads')->url($uploaded);
				            }
							$lang_data[$lang][$key] = $uploaded;
						} else {
							if(!is_array($lang_data[$lang][$key])) $lang_data[$lang][$key] = $value;
						}
	                }

					}
	            }
//dd($lang_data);
				$model->fill($lang_data);
            
	            $model->save();	
			}

           
            
            return [
                'status'  => 'success',
                'message' => trans('messages.save_ok'),
            ];
        } catch (ModelNotFoundException $e) {
            $message = trans('messages.record_not_found');
        } catch (Exception $e) {
            $message = trans('messages.update_error').': '.$e->getMessage();
        }
        
        return [
            'status'  => 'error',
            'message' => $message,
        ];
    }
    
    /**
     * Remove the specified resource from storage.
     * DELETE /variable/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $model = Variable::findOrFail($id);
            
            if ($model->is_hidden && !$this->user->hasAccess('superuser')) {
                //FlashMessages::add('error', trans('messages.record_not_found'));
                
                return redirect()->route('admin.variable.index');
            }
            
            if (!$model->delete()) {
                //FlashMessages::add("error", trans("messages.destroy_error"));
            } else {
                //FlashMessages::add('success', trans("messages.destroy_ok"));
            }
        } catch (ModelNotFoundException $e) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
            //FlashMessages::add("error", trans('messages.delete_error').': '.$e->getMessage());
        }
        
        return redirect()->route('admin.variable.index');
    }
    
    /**
     * set to template addition variables for add\update variable
     */
    private function _fillAdditionTemplateData()
    {
        $types = [];
        foreach (app(Variable::class)->getTypes() as $key => $type) {
            $types[$key] = trans('labels.variable_type_'.$type);
        }
        $this->data('types', $types);
    }
}