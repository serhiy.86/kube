<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SupportMail
 * @package App\Mail
 */
class SupportMail extends Mailable
{
    use Queueable, SerializesModels;
    
    /**
     * @var string
     */
    public $view = 'emails.support.message';

    private $email;
    private $msg;

    /**
     * Create a new message instance.
     *
     * @param $email
     * @param $message
     */
    public function __construct($email, $message)
    {
        $this->email = $email;
        $this->msg = $message;

	        $this->to('support@kube.ivestments');
			$this->replyTo($this->email);
            $this->subject('KUBE.investmets');
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with(
            [
                'email'     => $this->email,
                'msg'  => $this->msg,
            ]
        );
    }
}
