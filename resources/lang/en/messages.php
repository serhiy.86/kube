<?php

return array(
    'access_denied'                          => 'Access is denied',
    'user register error'                    => 'Registration error, please try again later.',
    'user register success message'          => 'Congratulations! You have successfully registered. An email has been sent to your email to confirm the activation of your profile',
    'user with such email already activated' => 'An account with this email has already been activated',
    'user with such email was banned'        => 'The account with this email has been banned',
    'user with such email was blocked'       => 'The account with this email has been blocked',
    'user with such email was not activated' => 'The account with this email has not yet been activated',
    'user with such email was not found'     => 'The account with the given email was not found',
    'validation_failed'                      => 'Validation error',
    'you already logged in'                  => 'You are already logged in',
);
