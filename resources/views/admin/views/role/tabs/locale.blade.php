<div class="form-group required @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', trans('admin_labels.name'), ['class' => "control-label"]) !!}

    {!! Form::text('name', null, ['placeholder' => trans('admin_labels.name'), 'class' => 'form-control input-sm', 'required' => true]) !!}
    {!! $errors->first('name', '<p class="help-block error">:message</p>') !!}
</div>

<div class="form-group @if (empty($model->toArray())) required @endif @if ($errors->has('slug')) has-error @endif">
    {!! Form::label('slug', trans('admin_labels.role_slug'), ['class' => "control-label"]) !!}

    {!! Form::text('slug', null, ['placeholder' => trans('admin_labels.slug'), 'class' => 'form-control input-sm', 'required' => empty($model->toArray()) ? true : false, 'readonly' => empty($model->toArray()) ? false : true]) !!}
    {!! $errors->first('slug', '<p class="help-block error">:message</p>') !!}

    @if (empty($model->toArray()))
        <div class="col-xs-12 col-sm-6 col-md-6 margin-top-4">
            <p class="help-block">(@lang('messages.role slug helper message'))</p>
        </div>
    @endif
</div>
