<?php namespace App\Http\Controllers\Frontend;

use App\Models\FaqCategory;
use App\Api\Transformers\FaqCategoryTransformer;

use App\Http\Controllers\ApiController;


class FaqCategoryController extends ApiController
{
    /**
     * Eloquent model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function model()
    {
        return new FaqCategory;
    }

    /**
     * Transformer for the current model.
     *
     * @return \League\Fractal\TransformerAbstract
     */
    protected function transformer()
    {
        return new FaqCategoryTransformer;
    }

    public function show($id=NULL)
    {
        $items = Faq::positionSorted()->visible()->get();

        return $this->respondWithCollection($items);
    }
}
