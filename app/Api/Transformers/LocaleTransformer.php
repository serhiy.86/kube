<?php

namespace App\Api\Transformers;

use League\Fractal\TransformerAbstract;

class LocaleTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $lang
     * @return array
     */
    public function transform($lang)
    {
        return $lang;
    }
}
