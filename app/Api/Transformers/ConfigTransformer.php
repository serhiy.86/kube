<?php namespace App\Api\Transformers;

use App\Models\Variable;
use League\Fractal\TransformerAbstract;

class ConfigTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     * @return array
     */
    public function transform(Variable $item)
    {
		if($item->multilingual) {
			$value = json_decode($item->text,true);
			$value = (is_array($value)) ? $value : $item->text;
		} else {
			$value  = $item->value;
		}
        return [
            'key'         => (string)$item->key,
            'name'        => (string)$item->name,
            'description' => (string)$item->description,
            'value'       => $value,
        ];
    }
}
