<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\ResponseFactory;
use Sentinel;
use Session;
use URL;

/**
 * Class AdminAuthenticate
 * @package App\Http\Middleware
 */
class AdminAuthenticate
{
    
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;
    
    /**
     * The response factory implementation.
     *
     * @var ResponseFactory
     */
    protected $response;
    
    /**
     * Create a new filter instance.
     *
     * @param  Guard           $auth
     * @param  ResponseFactory $response
     *
     */
    public function __construct(Guard $auth, ResponseFactory $response)
    {
        $this->auth = $auth;
        $this->response = $response;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Sentinel::check()) {
            Session::put('redirect', URL::full());
            
            return $request->ajax()
                ? $this->response->make('Unauthorized', 401)
                : $this->response->redirectToRoute('admin.auth.login');
        } else {
            $user = Sentinel::getUser();
            // Check if the user is in the administrator group
            if (!$user || !$user->hasAccess('administrator')) {
                Sentinel::logout();
                
                return $request->ajax()
                    ? $this->response->make('Unauthorized', 401)
                    : $this->response->redirectToRoute('admin.auth.login');
            }
            
            if ($redirect = Session::get('redirect')) {
                Session::forget('redirect');
                
                return $this->response->redirectTo($redirect);
            }
        }
        
        return $next($request);
    }
}
