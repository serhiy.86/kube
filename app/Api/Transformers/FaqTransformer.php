<?php
namespace App\Api\Transformers;

use App\Models\Faq;
use League\Fractal\TransformerAbstract;

class FaqTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(Faq $item)
    {
        return [
			    'id' => (int)$item->id,
          'faq_category_id' => (int)$item->faq_category_id,
          'question' => (string)$item->question,
          'answer' => (string)$item->answer,
        ];
    }
}
