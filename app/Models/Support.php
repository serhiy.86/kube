<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Support
 * @package App\Models
 */
class Support extends Model
{
	//use SoftDeletes;

	protected $table="feedback";

    /**
     * @var array
     */
    protected $fillable = [
	    'email',
		'message',
    ];
}
