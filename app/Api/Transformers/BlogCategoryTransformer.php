<?php namespace App\Api\Transformers;

use App\Models\BlogCategory;
use League\Fractal\TransformerAbstract;

class BlogCategoryTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(BlogCategory $item)
    {
        return [
          'id'		  => (int)$item->id,
    			'parent_id'		  => (int)$item->parent_id,
          'slug'    => (string)$item->slug,
          'title'    => (string)$item->name,
          'status'  => (boolean)$item->status,
        ];
    }
}
