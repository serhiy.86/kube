window.dataTablaReload = ($datatable) ->
  $datatable.DataTable().ajax.reload(null, false);

window.filterDataTable = ($table) ->
  $datatable = $('#' + $table.attr('id'))
  params = [];

  $('.datatable-filter').each () ->
    params.push($(this).attr('name') + '=' + $(this).val())

  url = $datatable.DataTable().ajax.url()

  url = $datatable.DataTable().ajax.url().split('?');
  url = url[0]  + '?' + params.join('&');

  $table.DataTable().ajax.url(url).load()

$(document).ready () ->
  $('table.dataTable').on  'draw.dt', () ->
    initCheckboxes()

  $(document).on "keyup", 'input[type=\'text\'].datatable-filter', ->
    filterDataTable($('#datatable1'))

  $(document).on "change", 'input[type=\'text\'].datatable-date-filter', ->
    filterDataTable($('#datatable1'))


  $(document).on "change", 'select.datatable-filter', ->
    filterDataTable($('#datatable1'))