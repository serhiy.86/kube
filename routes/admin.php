<?php

use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => 'admin',
        'as'     => 'admin.',
    ],
    function () {

		Route::group(
            [
                'prefix' => 'auth',
                'as'     => 'auth.',
            ],
            function () {
                Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);

                Route::post('login', ['as' => 'login.post', 'uses' => 'AuthController@postLogin']);

                // users

                Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);

            }
        );



        Route::group(
            [
                'middleware' => 'auth.admin',
            ],
            function () {
                Route::get('/', ['as' => 'home', 'uses' => 'DashboardController@index']);

                // roles
                Route::resource('role', 'RoleController');

                // users
                Route::get(
                    'user/find',
                    [
                        'middleware' => 'ajax',
                        'as'         => 'user.find',
                        'uses'       => 'UserController@find',
                    ]
                );
                Route::post(
                    'user/{id}/ajax_field',
                    [
                        'middleware' => ['ajax'],
                        'as'         => 'user.ajax_field',
                        'uses'       => 'UserController@localAjaxFieldChange',
                    ]
                );
                Route::get(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.get', 'uses' => 'UserController@getNewPassword']
                );
                Route::post(
                    'user/new_password/{id}',
                    ['as' => 'user.new_password.post', 'uses' => 'UserController@postNewPassword']
                );

                Route::resource('user', 'UserController', ['except' => 'delete']);


                // roles
                Route::resource('role', 'RoleController');


                

		// variables
                Route::post(
                    'variable/{id}/ajax_field',
                    array (

                        'as'         => 'variable.ajax_field',
                        'uses'       => 'VariableController@ajaxFieldChange',
                    )
                );
                Route::get(
                    'variable/value/index',
                    ['as' => 'variable.value.index', 'uses' => 'VariableController@indexValues']
                );
                Route::post(
                    'variable/value/update',
                    [

                        'as'         => 'variable.value.update',
                        'uses'       => 'VariableController@updateValue',
                    ]
                );
                Route::resource('variable', 'VariableController');
    });


                // files download
                Route::get(
                    'download-file',
                    ['as' => 'download.file', 'uses' => 'BackendController@downloadFile']
                );

    });
