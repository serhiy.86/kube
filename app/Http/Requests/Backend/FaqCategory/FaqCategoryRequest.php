<?php

namespace App\Http\Requests\Backend\FaqCategory;

use App\Http\Requests\BaseRequest;

/**
 * Class FaqCategoryRequest
 * @package App\Http\Requests\FaqCategory
 */
class FaqCategoryRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status'   => 'required|boolean',
            'position' => 'required|integer',
        ];
        
        $languageRules = [
            'name' => 'required',
        ];
        
        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }
        
        return $rules;
    }
}