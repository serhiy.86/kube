<?php namespace App\Api\Transformers;

use App\Models\Speaker;
use League\Fractal\TransformerAbstract;

class SpeakerTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(Speaker $item)
    {
        return [
            'id'         => (int)$item->id,
            'name'       => (string)$item->name,
            'speciality' => (string)$item->posada,
            'image'      => (string)$item->image,
        ];
    }
}
