<?php

namespace App\Jobs;

use App\Mail\Activation;
use App\Models\User;
use App\Mail\VerificationApprovedEmail;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendUserVerificationApprovedEmail
 * @package App\Jobs
 */
class SendUserVerificationApprovedEmail extends BaseJob
{

    /**
     * @var \App\Models\User
     */
    private $user;
    private $code;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\User $user
     */
    public function __construct(User $user, EloquentActivation $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new Activation($this->user, $this->code));
    }
}
