<?php

namespace App\Http\Requests\Backend\BlogCategory;

use App\Http\Requests\BaseRequest;

/**
 * Class BlogCategoryCreateRequest
 * @package App\Http\Requests\BlogCategory
 */
class BlogCategoryCreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status' => 'required|boolean',
            'slug' => 'unique:blog_categories,slug',
        ];

        $languageRules = [
            'title' => 'required',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }
}
