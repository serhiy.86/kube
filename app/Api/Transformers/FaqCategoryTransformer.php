<?php namespace App\Api\Transformers;

use App\Models\FaqCategory;
use League\Fractal\TransformerAbstract;

class FaqCategoryTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(FaqCategory $item)
    {
        return [
    			'id'		  => (int)$item->id,
          'slug'    => (string)$item->slug,
          'name'    => (string)$item->name,
          'position'    => (string)$item->position,
          'status'  => (boolean)$item->status,
        ];
    }
}
