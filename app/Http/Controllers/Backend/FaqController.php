<?php

namespace App\Http\Controllers\Backend;

use App\Models\Faq;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Faq\FaqRequest;
use App\Models\FaqCategory;
use DataTables;
//use FlashMessages;
use Illuminate\Http\Request;
//use Meta;
use Response;
use Yajra\DataTables\Html\Builder;

/**
 * Class FaqController
 * @package App\Http\Controllers\Backend
 */
class FaqController extends BackendController
{
    
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "faq";
    
    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'faq.read',
        'create'          => 'faq.create',
        'store'           => 'faq.create',
        'show'            => 'faq.read',
        'edit'            => 'faq.read',
        'update'          => 'faq.write',
        'destroy'         => 'faq.delete',
        'ajaxFieldChange' => 'faq.write',
    ];
    
    /**
     * Display a listing of the resource.
     * GET /faq
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */
    public function index(Request $request, Builder $builder)
    {
        if ($request->get('draw')) {
            $list = Faq::joinTranslations('faq', 'faq_translations', 'id', 'faq_id')
                ->select(
                    'faq.id as id',
                    'faq_translations.question as question',
                    'faq_translations.answer as answer',
                    'faq.status as status',
                    'faq.position as position'
                );
            
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'question',
                    function ($query, $keyword) {
                        return $query->whereRaw('faq_translations.question like ?', ['%'.$keyword.'%']);
                    }
                )
                ->filterColumn(
                    'answer',
                    function ($query, $keyword) {
                        return $query->whereRaw('faq_translations.answer like ?', ['%'.$keyword.'%']);
                    }
                )
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('faq.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->editColumn(
                    'question',
                    function ($model) {
                        return str_limit($model->question);
                    }
                )
                ->editColumn(
                    'answer',
                    function ($model) {
                        return str_limit(strip_tags($model->answer));
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'position',
                    function ($model) {
                        return view(
                            'partials.datatables.text_input',
                            ['model' => $model, 'type' => $this->module, 'field' => 'position']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'position', 'actions'])
                ->make();
        }
        
		$html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => trans('labels.id')],
                ['data' => 'question', 'name' => 'question', 'title' => trans('labels.question')],
                ['data' => 'answer', 'name' => 'answer', 'title' => trans('labels.answer')],
                ['data' => 'status', 'name' => 'status', 'title' => trans('labels.status')],
                ['data' => 'position', 'name' => 'position', 'title' => trans('labels.position')],
                ['data' => 'actions', 'name' => 'actions', 'title' => trans('labels.actions')],
            ])
			->parameters([
				'sPaginationType' => "bootstrap_alt",
				'drawCallback' => 'function() { initToggles(); }',
//								'language' => ["url" => '//cdn.datatables.net/plug-ins/1.10.20/i18n/'.config('laravellocalization.supportedLocales.'.$this->locale.'.name','Russian').'.json'],
			]);

        $this->data('page_title', trans('labels.all_faq'));
        $this->breadcrumbs(trans('labels.faq_list'));
        
        return $this->render('views.faq.index',compact('html'));
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /faq/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->_fillAdditionTemplateData();

        $this->data('model', new Faq);
        
        $this->data('page_title', trans('labels.faq_create'));
        
        $this->breadcrumbs(trans('labels.faq_create'));
        
        return $this->render('views.faq.create');
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /faq
     *
     * @param FaqRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FaqRequest $request)
    {
        $model = new Faq();
        
        $model->fill($request->all());
        
        $model->save();
        
        //FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.faq.index');
    }
    
    /**
     * Display the specified resource.
     * GET /faq/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     * GET /faq/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Response
     */
    public function edit($id)
    {
        $this->_fillAdditionTemplateData();
        $model = Faq::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq.index');
        }
        
        $this->data('page_title', trans('labels.faq_editing'));
        
        $this->breadcrumbs(trans('labels.faq_editing'));
        
        return $this->render('views.faq.edit', compact('model'));
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /faq/{id}
     *
     * @param  int            $id
     * @param FaqRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, FaqRequest $request)
    {
        $model = Faq::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq.index');
        }
        
        $model->update($request->all());
        
        //FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.faq.index');
    }
    
    /**
     * Remove the specified resource from storage.
     * DELETE /faq/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = Faq::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq.index');
        }
        
        $model->delete();
        
        //FlashMessages::add('success', trans("messages.destroy_ok"));
        
        return redirect()->route('admin.faq.index');
    }

    /**
     * set to template addition variables for add\update variable
     *
     */
    private function _fillAdditionTemplateData()
    {
        $faq_category =  FaqCategory::with('translations')
            ->visible()
            ->positionSorted()
            ->get()
            ->keyBy('id')
            ->map(
                function ($item) {
                    return $item->name;
                }
            )
            ->toArray();
        $this->data('faq_category', $faq_category);

    }
}