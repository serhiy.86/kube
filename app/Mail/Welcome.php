<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Welcome
 * @package App\Mail
 */
class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $view = 'emails.user.welcome';

    /**
     * @var \App\Models\User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;

        $this->to($user->email, $user->getFullName());
        $this->subject(trans('auth.welcome_email'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this;
    }
}
