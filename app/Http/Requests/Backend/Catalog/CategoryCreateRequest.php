<?php namespace App\Http\Requests\Backend\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use FlashMessages;
use Config;

class CategoryCreateRequest extends FormRequest
{

    protected $dontFlash = ['imageUpload'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status'   => 'required|boolean',
            'slug'     => 'unique:categories,slug,NULL,id',
            'position' => 'required|integer',
        ];

        $languageRules = [
            'name'             => 'required',
            'description'      => '',
            'meta_keywords'    => '',
            'meta_title'       => '',
            'meta_description' => '',
        ];

        foreach (Config::get('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param  array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        FlashMessages::add("error", trans("messages.validation_failed"));

        return parent::response($errors);
    }
}
