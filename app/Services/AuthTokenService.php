<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthTokenService
{
    /**
     * Get a JWT token via given credentials.
     *
     * @param  array  $credentials
     *
     * @return mixed
     */
    public function login($credentials)
    {
        if ($token = $this->guard()->attempt($credentials)) {
            return $token;
        }

        return false;
    }

    /**
     * Get a JWT token via given model.
     *
     * @param  JWTSubject  $model
     *
     * @return mixed
     */
    public function fromUser($model)
    {
        if ($token = $this->guard()->login($model)) {
            return $token;
        }

        return false;
    }

    /**
     * Get the authenticated User
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->guard()->user();
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return boolen
     */
    public function logout()
    {
        $this->guard()->logout();

        return true;
    }

    /**
     * Refresh a token.
     *
     * @return token
     */
    public function refresh()
    {
        return $this->guard()->refresh();
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}
