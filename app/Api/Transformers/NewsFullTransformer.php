<?php namespace App\Api\Transformers;

use App\Models\News;
use League\Fractal\TransformerAbstract;

class NewsFullTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     * @return array
     */
    public function transform(News $item)
    {
        return [
            'id'               => (int)$item->id,
            'image'            => (string)asset($item->image),
            'view_count'       => (int)$item->view_count,
            'date'             => (string)$item->date,
            'name'             => (string)$item->name,
            'short_content'    => (string)$item->short_content,
            'content'          => (string)$item->content,
            'meta_title'       => (string)$item->meta_title,
            'meta_keywords'    => (string)$item->meta_keywords,
            'meta_description' => (string)$item->meta_description,
        ];
    }
}
