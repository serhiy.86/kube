<?php

namespace App\Http\Controllers\Api;

use App\Events\ChangePassword;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\PasswordChangeFromCodeRequest;
use App\Http\Requests\User\PasswordRemindRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Jobs\SendPasswordReminderEmail;
use App\Jobs\SendUserVerificationApprovedEmail;
use App\Jobs\SendUserWelcomeEmail;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\ApiController;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Activation;
use Sentinel;

class AuthController extends ApiController
{

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $user = $this->user_service->create($data);

        $activation = Activation::create($user);

        dispatch(new SendUserVerificationApprovedEmail($user, $activation))->onQueue('users');

        try {
            if (!$token = $this->auth->fromUser($user)) {
                return response([
                    'message' => 'Unauthorized',
                    'error_code' => 401
                ]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response([
                'message' => 'Failed to login, please try again.',
                'error_code' => 500
            ]);
        }

        return response([
            'message' => trans('messages.user register success message'),
            'error_code' => 0
        ]);
    }

    public function register_complete(Request $request)
	{
		$email = $request->input('email');

        $user = Sentinel::findByCredentials(['email' => $email]);

		if (!$user || $user->reg_complete) {
            $status = 'Sorry your email cannot be identified';
        } else {
			$user->load('info');
			$user->update(['reg_complete'=>true]);
			$user->info->fill(
				$request->only(['country','type','investments'])
			);
			$user->info->save();
			return response([
	            'message' => trans('messages.user register success message'),
	            'error_code' => 0
	        ]);
		}

		return response([
            'message' => $status,
            'error_code' => 422
        ], 422);
	}


    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->auth->login($credentials)) {
            return $this->auth->respondWithToken($token);
        }
        return response([
            'message' => trans('messages.access_denied'),
            'error_code' => 418
        ],418);
    }

    public function activation(Request $request)
    {
        $email = $request->input('email');
        $code = $request->input('activation_code');

        $user = Sentinel::findByCredentials(['email' => $email]);

        if (!$user) {
            $status = 'Sorry your email cannot be identified';
        } else {
            if (Activation::complete($user, $code)) {
                $status = "Your e-mail is verified.";

                dispatch(new SendUserWelcomeEmail($user))->onQueue('users');

                if ($token = $this->auth->fromUser($user)) {
                    return $this->auth->respondWithToken($token);
                }
            } else {
                if (Activation::completed($user)) {
                    $status = "Your e-mail is already verified.";
                } else {
                    $status = "Invalid activation code";
                }
            }
        }

        return response([
            'message' => $status,
            'error_code' => 422
        ], 422);
    }

    public function reactivation(Request $request)
    {
        $email = $request->input('email');

        $user = Sentinel::findByCredentials(['email' => $email]);

        if ($user && !Activation::completed($user)) {

            $activation = Activation::exists($user);
            if($activation) {
				Activation::remove($user);
			}

            $activation = Activation::create($user);

            dispatch(new SendUserVerificationApprovedEmail($user, $activation))->onQueue('users');

            return response([
                'message' => 'activation',
                'error_code' => 0
            ], 200);

        }

        return response([
            'message' => 'Invalid credentials.',
            'error_code' => 422
        ], 422);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->auth->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->auth->respondWithToken($this->auth->refresh());
    }

    public function remindPassword(PasswordRemindRequest $request)
    {
        $credentials = $request->only('email');
        $user = Sentinel::findByCredentials($credentials);
        $reminder = Reminder::create($user);

        dispatch(new SendPasswordReminderEmail($user, $reminder->code))->onQueue('users');

        return response()->json(['message' => 'Successfully send reset email']);
    }

    public function resetPassword(PasswordChangeFromCodeRequest $request)
    {
        $credentials = $request->only('email', 'code', 'password');

        $user = Sentinel::findByCredentials(['email' => $credentials['email']]);

        $reminder = Reminder::complete($user, $credentials['code'], $credentials['password']);

        if (!$reminder) {
            return response()->json(['message' => 'Invalid credentials.']);
        }

        event(new ChangePassword($user));

        return response()->json(['message' => 'Successfully reset password']);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->auth->getUser());
    }

}
