<?php

namespace App\Providers;

/**
 * Part of the Cent package.
 *
 * @package    Cent
 * @version    1.0.1
 * @author     joshwhatk
 * @license    MIT
 * @link       http://jwk.me
 */

use Illuminate\Support\Facades\Auth;
use App\Guard\SentinelGuard;
use Illuminate\Support\ServiceProvider;

class SentinelGuardServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        Auth::extend('sent', function () {
            return new SentinelGuard();
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
