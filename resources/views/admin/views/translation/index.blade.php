@extends('layouts.editable')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="translations-table">

                        <form action="{!! route('admin.translation.update', ['group' => $group]) !!}" method="post"
                              role="form"
                              class="without-js-validation">

                            {!! csrf_field() !!}

                            <input type="hidden" name="page" value="{!! $page !!}">

                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td colspan="{!! count($selected_locales) + 1 !!}">
                                        @include('translation.partials.buttons')
                                    </td>
                                </tr>

                                <tr class="hidden">
                                    <td colspan="{!! count($selected_locales) + 1 !!}">
                                        <div class="text-center translations-filter margin-top-10">
                                            <div class="cols-xs-12 col-sm-3 col-md-2 col-lg-1 line-height-27">
                                                {!! Form::label('selected_locales', trans('admin_labels.locales'), ['class' => 'control-label font-size-14']) !!}
                                            </div>

                                            <div class="form-group cols-xs-12 col-sm-6 col-md-4 col-lg-3">
                                                {!! Form::select('selected_locales[]', $locales, $selected_locales, ['id' => 'selected_locales', 'class' => 'form-control select2 input-sm', 'aria-hidden' => 'true', 'multiple' => 'multiple']) !!}
                                                <br>
                                            </div>

                                            <div class="cols-xs-12 col-sm-1">
                                                {!! Form::button(trans('admin_labels.select'), ['class' => 'btn btn-success btn-sm btn-flat']) !!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="{!! count($selected_locales) + 1 !!}">
                                        <div class="text-center">
                                            {!! $list->links() !!}
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th style="width: 30%!important;">@lang('admin_labels.key')</th>

                                    @foreach($selected_locales as $locale)
                                        <th style="width: 70%!important;">{!! trans('admin_labels.tab_' . $locale) !!}</th>
                                    @endforeach
                                </tr>
                                </tbody>

                                @foreach($list as $key => $items)
                                    <tr>
                                        <td style="width: 30%!important;">
                                            {!! $key !!}
                                        </td>

                                        @foreach($selected_locales as $locale)
                                            <td style="width: 70%!important;"
                                                class="form-group
                                            @if ($errors->has($locale.'.'.$key)) has-error @endif">
                                            <textarea
                                                    name="{!! $locale !!}[{!! $key !!}]"
                                                    id="{!! $locale !!}_{!! str_replace(' ', '_', $key) !!}"
                                                    class="form-control input-sm"
                                                    style="max-width: 95%!important; min-width: 95%!important; width: 95%!important;"
                                            >{!! isset($items[$locale]) ? $items[$locale] : null !!}</textarea>
                                            </td>
                                            @if ($group == 'texts')
                                                @include('partials.tabs.ckeditor', ['id' => $locale.'_'.str_replace(' ', '_', $key)])
                                            @endif
                                        @endforeach
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="{!! count($selected_locales) + 1 !!}">
                                        <div class="text-center">
                                            {!! $list->links() !!}
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="{!! count($selected_locales) + 1 !!}">
                                        @include('translation.partials.buttons')
                                    </td>
                                </tr>
                            </table>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection