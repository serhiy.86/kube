<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

/**
 * Class PasswordChangeFromCodeRequest
 * @package App\Http\Requests\Backend\User
 */
class PasswordChangeRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'old_password' => 'required|min:' . config('auth.passwords.min_length'),
            'password'     => 'required|confirmed|min:' . config('auth.passwords.min_length'),
        ];
    }
}
