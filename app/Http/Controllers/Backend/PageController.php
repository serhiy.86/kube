<?php

namespace App\Http\Controllers\Backend;

use App\Models\Page;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\Page\PageCreateRequest;
use App\Http\Requests\Backend\Page\PageUpdateRequest;
//use App\Models\PageCustomField;
use App\Services\PageService;
use DataTables;
//use FlashMessages;
use Illuminate\Http\Request;
use Meta;
use Yajra\DataTables\Html\Builder;

/**
 * Class PageController
 * @package App\Http\Controllers\Backend
 */
class PageController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "page";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'page.read',
        'create'          => 'page.create',
        'store'           => 'page.create',
        'show'            => 'page.read',
        'edit'            => 'page.read',
        'update'          => 'page.write',
        'destroy'         => 'page.delete',
        'ajaxFieldChange' => 'page.write',
    ];

    /**
     * @var PageService
     */
    private $pageService;


    /**
     * Display a listing of the resource.
     * GET /page
     *
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */

	public function index(Builder $builder)
    {
		if (request()->ajax()) {
            
           $list = Page::joinTranslations('pages', 'page_translations')
                ->select(
                    'pages.id as id',
                    'page_translations.name as name',
                    'pages.status as status',
                    'pages.position as position',
                    'pages.parent_id as parent_id',
                    'pages.slug as slug'
                );
            
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        $query->whereRaw("page_translations.name like ?", ["%{$keyword}%"]);
                    }
                )
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('pages.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'front_link' => false, 'without_delete'=>true, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'actions'])
                ->make();
        }
        
		
		$html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => trans('labels.id')],
                ['data' => 'name', 'name' => 'name', 'title' => trans('labels.name')],
                ['data' => 'status', 'name' => 'status', 'title' => trans('labels.status')],
                ['data' => 'position', 'name' => 'position', 'title' => trans('labels.position')],
	                ['data' => 'actions', 'name' => 'actions', 'title' => trans('labels.actions')],
            ])
			->parameters([
				'sPaginationType' => "bootstrap_alt",
				'drawCallback' => 'function() { initToggles(); }',
				'language' => ["url" => '//cdn.datatables.net/plug-ins/1.10.20/i18n/'.config('laravellocalization.supportedLocales.'.$this->locale.'.name','Russian').'.json'],
			]);
        
        $this->data('page_title', trans('labels.pages'));

        return $this->render('views.page.index')
			->with('html',$html);
    }

    /**
     * Show the form for creating a new resource.
     * GET /page/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->_fillAdditionTemplateData();

        $this->data('model', new Page);

        $this->data('page_title', trans('labels.page_create'));

        $this->breadcrumbs(trans('labels.page_create'));

        return $this->render('views.page.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /page
     *
     * @param PageCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(PageCreateRequest $request, PageService $pageService)
    {
        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;

        $model = new Page($input);

        $model->save();

        $pageService->setExternalUrl($model);

        //FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.page.index');
    }

    /**
     * Display the specified resource.
     * GET /page/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /page/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        $model = Page::/*with('custom_fields')->*/findOrFail($id);

        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.page.index');
        }

        $this->data('page_title', '"'.$model->name.'"');

        $this->breadcrumbs(trans('labels.page_editing'));

        $this->_fillAdditionTemplateData($model);

        return $this->render('views.page.edit')->with('model',$model);
    }

    /**
     * Update the specified resource in storage.
     * PUT /page/{id}
     *
     * @param  int              $id
     * @param PageUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     */
    public function update($id, PageUpdateRequest $request)
    {
        $model = Page::/*with('custom_fields')->*/findOrFail($id);

        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.page.index');
        }

        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;

        $model->update($input);

        $settings = $request->only(config('translatable.locales'));

        if(isset($model->custom_fields)) {
			$custom_fields = array_keys($model->custom_fields->fields); //$model->custom_fields->getFieldsKeys();

            foreach ($settings as $lang => $fields) {

                foreach ($fields as $key => $value) {
                    if(in_array($key, $custom_fields)){
						if(is_object($value)) {
							$uploaded = $value->storeAs('page/'.$model->id, $value->hashName().'.'.$value->getClientOriginalName() , 'uploads');
				            if ($uploaded) {
				                $uploaded = \Storage::disk('uploads')->url($uploaded);
				            }
							$model->setting()->updateOrCreate(['key' => $key], [$lang => ['key' => $key, 'value' => $uploaded]]);
						} else {
	                        $model->setting()->updateOrCreate(['key' => $key], [$lang => ['key' => $key, 'value' => $value]]);
						}
                    }
                }
            }
        }

        //FlashMessages::add('success', trans('messages.save_ok'));

        return redirect()->route('admin.page.index');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /page/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = Page::findOrFail($id);

        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.page.index');
        }

        $model->delete();

        //FlashMessages::add('success', trans("messages.destroy_ok"));

        return redirect()->route('admin.page.index');
    }

    /**
     * set to template addition variables for add\update page
     *
     * @param \App\Models\Page|null $model
     */
    private function _fillAdditionTemplateData($model = null)
    {
        $this->data(
            'templates',
            /*get_templates(
                base_path('resources/themes/'.config('app.theme').'/views/'.$this->module.'/templates'),
                true
            )*/
			[]
        );
        /*$page_types =  PageCustomField::get()
            ->keyBy('id')
            ->map(
                function ($item) {
                    return $item->type;
                }
            )
            ->toArray();
        $page_types = [null => 'default'] + $page_types;
		*/
		$page_types = [null => 'default'];
        $page_setting = null;
        if(isset($model)) {
            $page_setting = $model->setting()->get()->keyBy('key')->map(function ($item, $key) {
                $item->value = $item->getTranslationsArray();
                return $item;
            })->toArray();
        }
        $this->data('page_setting', $page_setting);
        $this->data('page_types', $page_types);

    }
}
