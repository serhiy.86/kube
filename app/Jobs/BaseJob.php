<?php

namespace App\Jobs;

use App\Jobs\Traits\FailedNotificatable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

/**
 * Class BaseJob
 * @package App\Jobs
 */
abstract class BaseJob implements ShouldQueue
{
    
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, FailedNotificatable;
    
    /**
     * @var int
     */
    public $allowed_attempts = 5;
    
    /**
     * Notify admin about the Job was stuck
     */
    public function preventLock()
    {
        if ($this->attempts() > $this->allowed_attempts - 1) {
            Log::error(
                static::class.' error. Job is stuck. Attempts = '.$this->attempts(),
                [
                    'payload' => func_get_args(),
                ]
            );
        }
    }
}