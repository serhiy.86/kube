window.select2Options =
  language: lang
  minimumResultsForSearch: window.select2_per_page

window.initSelects = ($area) ->
  $area = $area || null

  if $area
    $area.find('span.select2').remove()

    $area.find("select.select2").each () ->
      $(this).select2(select2Options)
  else
    $('select.select2').each () ->
      $(this).select2(select2Options)

  initAjaxSelects($area)

window.initAjaxSelects = ($area) ->
  $area = $area || null

  if $area
    $selects = $area.find('select.select2-ajax')
  else
    $selects = $('select.select2-ajax')

  $selects.each () ->
    selectAjaxOptions =
      ajax:
        url: $(this).data('url')
        dataType: 'json'
        delay: 250
        data: (params) ->
          return {
            search_text: params.term
            page: params.page
          }
        processResults: (data, params) ->
          unless data.status is 'success'
            message.show lang_error, 'error'

            return {
              results: []
            }

          params.page = params.page || 1;

          unless data.items.total is undefined
            more = data.items.total >= params.page * window.select2_per_page
          else
            more = data.items.length >= window.select2_per_page

          items = data.items.data || data.items

          results = $.map(items, (item) ->
            {
              text: item.name || (item.title || item.full_name)
              id: item.id || item.name
            }
          )

          return {
            results: results
            pagination:
              more: more
          }
      minimumInputLength: 0

    select2Options = $.extend({}, window.select2Options, selectAjaxOptions)

    $(this).select2(select2Options)

$(document).on "ready", () ->
  $(document).on "select2:select", (e) ->
    $row = $(e.target).closest('.field-row')

    if $row.length
      input = $row.find('.input-mask')
      input.inputmask('remove')
      _class = input.attr('class').replace /inputmask-\d/i, ''
      input.attr('class', _class)
      input.addClass('inputmask-' + e.params.data.id)
      initInputMask()