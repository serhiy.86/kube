<?php

namespace App\Http\Requests\Backend\Speaker;

use App\Http\Requests\BaseRequest;

/**
 * Class SpeakerCreateRequest
 * @package App\Http\Requests\Backend\Speaker
 */
class SpeakerCreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'   => 'required',
            'posada' => 'required',
        ];

        return $rules;
    }
}
