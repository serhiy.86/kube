<?php namespace App\Api\Transformers;

use App\Models\Blog;
use League\Fractal\TransformerAbstract;

class BlogTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(BLog $item)
    {
        return [
			    'id' => (int)$item->id,
          'title' => (string)$item->title,
          'slug' => (string)$item->slug,
          'short_description' => (string)$item->short_description,
          'text' => (string)$item->text,
          'image' => (string)$item->image,
          'image_show' => (boolean)$item->image_show,
          'h1' => (string)$item->h1,
          'meta_title' => (string)$item->meta_title,
          'meta_description' => (string)$item->meta_description,
          'meta_keyword' => (string)$item->meta_keyword,
          'published' => (string)$item->published,
          'for_authorized' => (boolean)$item->for_authorized,
          'text' => (string)$item->text,
        ];
    }
}
