<?php

return [
    'main_page' => env('SITE_MAIN_PAGE_LINK'),
    'reset_password' => env('SITE_MAIN_PAGE_LINK') . '/restore',
];
