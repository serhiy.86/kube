<?php

namespace App\Http\Requests\Backend\Speaker;

use App\Http\Requests\BaseRequest;

/**
 * Class SpeakerUpdateRequest
 * @package App\Http\Requests\Backend\Speaker
 */
class SpeakerUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'   => 'required',
            'posada' => 'required',
        ];

        return $rules;
    }
}
