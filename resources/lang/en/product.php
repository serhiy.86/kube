<?php

return [
  'head'                             => 'Products',
  'title'                            => 'Title',
  'body'                             => 'Body',
  'slug'                             => 'Slug',
  'excerpt'                          => 'Except',
  'image'                            => 'Photo product',
  'price'                            => 'Price product',
  'price_title'                      => 'Price name product',
  'rating'                           => 'Rating product',
  'trailer'                          => 'Trailer product',
  'count_students_end'               => 'Count students product',
  'rating'                           => 'Rating product',
  'meta_title'                       => 'Meta Title',
  'meta_description'                 => 'Meta Description',
  'meta_keywords'                    => 'Meta keywords',
  'status'                           =>'Status',
  'author_id'                        =>'Autor',
  'speaker_id'                       =>'Speaker',
  'catalog_filter_id'                =>'Catalog filter',
];
