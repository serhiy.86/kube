<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Sentinel;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('users')->truncate();
		DB::table('roles')->truncate();
		DB::table('role_users')->truncate();

		$role = [
			'name' => 'Administrator',
			'slug' => 'administrator',
			'permissions' => [
				'admin' => true,
			]
		];

		$adminRole = Sentinel::getRoleRepository()->createModel()->fill($role)->save();

		$admin = [
			//'user_name'=> 'admin',
			'email'    => 'admin@admin.com',
			'password' => 'admin',
			'permissions' => [
				'superuser' => true,
			]
		];

		$users = [

		];

		$adminUser = Sentinel::registerAndActivate($admin);
		$adminUser->roles()->attach($adminRole);

		if(!empty($users)) foreach ($users as $user)
		{
			Sentinel::registerAndActivate($user);
		}
	}

}
