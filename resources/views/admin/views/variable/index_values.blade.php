@extends('layouts.editable')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="variables-table margin-top-10">

                @foreach($list as $item)

                    @if (!$item->is_hidden || ($item->is_hidden && $user->hasAccess('superuser')))
                        <div class="box @if ($item->is_hidden) box-danger @else box-primary @endif">
                            {!! Form::model($item, ['role' => 'form', 'method' => 'post', 'route' => ['admin.variable.value.update'],'enctype'=>'multipart/form-data', 'class' => 'variable-value-form form-horizontal']) !!}

                            <input type="hidden" name="variable_id" value="{!! $item->id !!}">

                            <div class="box-body">
                                <input type="hidden" name="type" value="{!! $item->type !!}">
                                <input type="hidden" name="multilingual" value="{!! $item->multilingual !!}">

                                <div class="col-xs-12 col-sm-4 col-md-3 text-right">
                                    <label class="control-label text-right">
                                        {!! $item->name !!}
                                    </label>
                                    <div>{!! $item->description !!}</div>
                                    <br>
                                </div>

                                <div class="col-xs-12 col-sm-8 col-md-9">
                                    @include('views.variable.types.'.$item->getStringType())

                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                                            {!! Form::select('status', ['1' => trans('labels.status_on'), '0' => trans('labels.status_off')], null, ['id' => 'status', 'class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="row form-group">
                                        @if ($user->hasAccess('variablevalue.write'))
                                            <div class="col-md-4 pull-right ta-right">
                                                {!! Form::submit(trans('labels.save'), ['class' => 'btn btn-success btn-flat save-variable-value']) !!}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            {!! Form::close() !!}
                        </div>
                    @endif

                @endforeach

            </div>
        </div>
    </div>

@endsection