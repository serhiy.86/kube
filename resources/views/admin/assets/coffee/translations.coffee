$(document).ready ->
  $(document).on "click", '.translations-filter .btn', () ->
    unless $(this).find('loading').length
      new Loading $(this)

    locales = $(this).closest('.translations-filter').find('select').val()
    
    window.location.href = window.location.pathname + '?locales=' + locales.join(',')

  console.log "init translations"