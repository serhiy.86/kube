<?php

namespace App\Http\Requests\Variable;

use App\Http\Requests\BaseRequest;
use App\Models\Variable;

/**
 * Class VariableCreateRequest
 * @package App\Http\Requests\Variable
 */
class VariableCreateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'         => 'required|in:'.implode(',', array_keys(app(Variable::class)->getTypes())),
            'key'          => 'required|unique:variables,key',
            'name'         => 'required',
            'multilingual' => 'required|boolean',
        ];
    }
}