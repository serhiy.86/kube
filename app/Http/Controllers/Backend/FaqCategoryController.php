<?php

namespace App\Http\Controllers\Backend;

use App\Models\FaqCategory;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Requests\Backend\FaqCategory\FaqCategoryRequest;
use DataTables;
//use FlashMessages;
use Illuminate\Http\Request;
//use Meta;
use Response;
use Yajra\DataTables\Html\Builder;

/**
 * Class FaqCategoryController
 * @package App\Http\Controllers\Backend
 */
class FaqCategoryController extends BackendController
{
    
    use AjaxFieldsChangerTrait;
    
    /**
     * @var string
     */
    public $module = "faq_category";
    
    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'faq_category.read',
        'create'          => 'faq_category.create',
        'store'           => 'faq_category.create',
        'show'            => 'faq_category.read',
        'edit'            => 'faq_category.read',
        'update'          => 'faq_category.write',
        'destroy'         => 'faq_category.delete',
        'ajaxFieldChange' => 'faq_category.write',
    ];
    
    
    /**
     * Display a listing of the resource.
     * GET /faq_category
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View|\Response
     * @throws \Exception
     */
    public function index(Request $request, Builder $builder)
    {
        if ($request->ajax()) {
            $list = FaqCategory::joinTranslations('faq_category', 'faq_category_translations', 'id', 'faq_category_id')
                ->select(
                    'faq_category.id as id',
                    'faq_category_translations.name as name',
                    'faq_category.status as status',
                    'faq_category.position as position'
                );
            
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        return $query->whereRaw('faq_category_translations.name like ?', ['%'.$keyword.'%']);
                    }
                )
                ->filterColumn(
                    'actions',
                    function ($query, $keyword) {
                        $query->whereRaw('faq_category.id like ?', ['%'.$keyword.'%']);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'position',
                    function ($model) {
                        return view(
                            'partials.datatables.text_input',
                            ['model' => $model, 'type' => $this->module, 'field' => 'position']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'position', 'actions'])
                ->make();
        }
        
		$html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => trans('labels.id')],
                ['data' => 'name', 'name' => 'name', 'title' => trans('labels.name')],
                ['data' => 'status', 'name' => 'status', 'title' => trans('labels.status')],
                ['data' => 'position', 'name' => 'position', 'title' => trans('labels.position')],
                ['data' => 'actions', 'name' => 'actions', 'title' => trans('labels.actions')],
            ])
			->parameters([
				'sPaginationType' => "bootstrap_alt",
				'drawCallback' => 'function() { initToggles(); }',
//								'language' => ["url" => '//cdn.datatables.net/plug-ins/1.10.20/i18n/'.config('laravellocalization.supportedLocales.'.$this->locale.'.name','Russian').'.json'],
			]);

        $this->data('page_title', trans('labels.all_faq_category'));
        $this->breadcrumbs(trans('labels.faq_category_list'));
        
        return $this->render('views.faq_category.index',compact('html'));
    }
    
    /**
     * Show the form for creating a new resource.
     * GET /faq_category/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new FaqCategory());
        
        $this->data('page_title', trans('labels.faq_category_create'));
        
        $this->breadcrumbs(trans('labels.faq_category_create'));
        
        return $this->render('views.faq_category.create');
    }
    
    /**
     * Store a newly created resource in storage.
     * POST /faq_category
     *
     * @param FaqCategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FaqCategoryRequest $request)
    {
        $model = new FaqCategory();
        
        $model->fill($request->all());
        
        $model->save();
        
        //FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.faq_category.index');
    }
    
    /**
     * Display the specified resource.
     * GET /faq_category/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return $this->edit($id);
    }
    
    /**
     * Show the form for editing the specified resource.
     * GET /faq_category/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Response
     */
    public function edit($id)
    {
        $model = FaqCategory::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq_category.index');
        }
        
        $this->data('page_title', trans('labels.faq_category_editing'));
        
        $this->breadcrumbs(trans('labels.faq_category_editing'));
        
        return $this->render('views.faq_category.edit', compact('model'));
    }
    
    /**
     * Update the specified resource in storage.
     * PUT /faq_category/{id}
     *
     * @param  int            $id
     * @param FaqCategoryRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, FaqCategoryRequest $request)
    {
        $model = FaqCategory::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq_category.index');
        }
        
        $model->update($request->all());
        
        //FlashMessages::add('success', trans('messages.save_ok'));
        
        return redirect()->route('admin.faq_category.index');
    }
    
    /**
     * Remove the specified resource from storage.
     * DELETE /faq_category/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $model = FaqCategory::find($id);
        
        if (!$model) {
            //FlashMessages::add('error', trans('messages.record_not_found'));
            
            return redirect()->route('admin.faq_category.index');
        }
        
        $model->delete();
        
        //FlashMessages::add('success', trans("messages.destroy_ok"));
        
        return redirect()->route('admin.faq_category.index');
    }
}