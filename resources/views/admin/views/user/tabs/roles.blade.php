<div class="box-body">
    @foreach($roles as $key => $role_name)
        <div class="form-group col-sm-12">
            <label class="checkbox-label" for="'roles[{!! $key !!}]'">
                {!! Form::checkbox('roles['.$key.']', $key,
                !empty($user_roles) ? in_array($key, $user_roles) :
                    (Request::old('roles['.$key.']') ?: false),
                array('class' => 'square')) !!}

                <span class="title">{!! $role_name !!}</span>
            </label>
        </div>
    @endforeach

    @if ($errors->has('roles'))
        <div class="row has-error">
            <div class="col-xs-12">
                {!! $errors->first('roles', '<p class="help-block error">:message</p>') !!}
            </div>
        </div>
    @endif
</div>
