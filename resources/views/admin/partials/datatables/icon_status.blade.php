@php($value = isset($value) ? $value : $model->{$field})

<div class="col-xs-2 col-xs-offset-5">
    @if ($value == true)
        <i class="glyphicon glyphicon-ok text-green"></i>
    @else
        <i class="glyphicon glyphicon-remove text-red"></i>
    @endif
</div>