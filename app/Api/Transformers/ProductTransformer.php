<?php namespace App\Api\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     *
     * @return array
     */
    public function transform(Product $item)
    {
        return [
			    'id' => (int)$item->id,
          'slug' => (string)$item->slug,
          'status' => (string)$item->status,
          'title' => (string)$item->title,
          'short_content' => (string)$item->short_content,
          'content' => (string)$item->content,
          'content_fro_whom' => (string)$item->content_fro_whom,
          'content_benefit' => (string)$item->content_benefit,
          'content_program' => (string)$item->content_program,
          'image' => (string)$item->image,
          'price' => (int)$item->price,
          'price_title' => (string)$item->price_title,
          'trailer' => (string)$item->trailer,
          'count_students_end' => (int)$item->count_students_end,
          'rating' => (int)$item->rating,
          'h1' => (string)$item->h1,
          'meta_title' => (string)$item->meta_title,
          'meta_description' => (string)$item->meta_description,
          'meta_keyword' => (string)$item->meta_keyword,
        ];
    }
}
