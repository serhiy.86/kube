<?php

namespace App\Http\Requests\Backend\BlogCategory;

use App\Http\Requests\BaseRequest;

/**
 * Class BlogCategoryUpdateRequest
 * @package App\Http\Requests\BlogCategory
 */
class BlogCategoryUpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('blog_category');
        $rules = [
            'status' => 'required|boolean',
            'slug' => 'unique:blog_categories,slug,'.$id.',id',
        ];

        $languageRules = [
            'title' => 'required',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }

        return $rules;
    }
}
