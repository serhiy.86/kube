<?php

namespace App\Mail;

use App\Models\User;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class Activation
 * @package App\Mail
 */
class Activation extends Mailable
{

    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $view = 'emails.user.activation';

    /**
     * @var \App\Models\User
     */
    public $user;

    /**
     * @var \Cartalyst\Sentinel\Activations\EloquentActivation
     */
    public $activation;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\User                                 $user
     * @param \Cartalyst\Sentinel\Activations\EloquentActivation $activation
     */
    public function __construct(User $user, EloquentActivation $activation)
    {
        $this->user = $user;
        $this->activation = $activation;

        $this->to($user->email, $user->getFullName());
        $this->subject(trans('auth.activation_email'));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->with(
            [
                'full_name' => $this->user->getFullName(),
                'link'      => config('links.main_page') . '/activation-email?activation_code=' . $this->activation->code . '&email=' . $this->user->email,
            ]
        );
    }
}
