<?php namespace App\Http\Controllers\Backend\Catalog;

use App\Classes\ImageUploader;
use App\Models\Category;
use DB;
use Event;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use Redirect;
use Illuminate\Contracts\Routing\ResponseFactory;
use Yajra\DataTables\Html\Builder;

use App\Http\Controllers\Backend\BackendController;
use App\Http\Requests\Backend\Catalog\CategoryUpdateRequest;
use App\Http\Requests\Backend\Catalog\CategoryCreateRequest;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Backend
 */
class CategoryController extends BackendController
{
    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "catalog.category";

	public $separator = '-------------------------------------------';

    /**
     * @var array
     */
    public $accessMap = [
        'index'               => 'catalog.category.read',
        'show'                => 'catalog.category.read',
        'create'              => 'catalog.category.write',
        'store'               => 'catalog.category.write',
        'edit'                => 'catalog.category.write',
        'update'              => 'catalog.category.write',
        'ajaxAutoImageUpload' => 'catalog.category.write',
        'destroy'             => 'catalog.category.delete',
        'postStatus'          => 'catalog.category.status',
        'postPosition'        => 'catalog.category.position',
    ];

    /**
     * @return $this
     */
    public function index(Builder $builder)
    {
        $selectedParentCategory = request()->get('category',-1);

		//Category::fixTree();

        if (request()->ajax()) {
            $list = Category::joinTranslations()->select(
                'categories.id',
                'category_translations.name',
                'parent_id',
                'status',
                'position',
                'slug'
            );
            if($selectedParentCategory) {
				$list->descendantsAndSelf($selectedParentCategory);
            }

            return $dataTables = DataTables::of($list)
                ->filterColumn(
					'id',
                    function ($query, $keyword) {
                        $query->whereRaw("categories.id like ?", ["%{$keyword}%"]);
                    })
                ->filterColumn(
					'name',
					function ($query, $keyword) {
                        $query->whereRaw("category_translations.name like ?", ["%{$keyword}%"]);
                    })
				->editColumn(
                    'name',
                    function ($model) {
                        return substr('-------------------------------------------',0,$model->depth).' '.$model->name;
                    }
                )
                ->editColumn(
                    'parent_id',
                    function ($model) {
                        if ($parent = Category::find($model->parent_id)) {
                            return $parent->name;
                        }

                        return '';
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => 'catalog.category', 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'position',
                    function ($model) {
                        return view(
                            'partials.datatables.text_input',
                            ['model' => $model, 'type' => 'catalog.category','field'=>'position']
                        )->render();
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'front_link' => true, 'type' => 'catalog.category']
                        )->render();
                    }
                )
				->rawColumns(['status','position', 'actions'])
                ->make();
        }

			$html = $builder->columns([
                ['data' => 'id', 'name' => 'id', 'title' => trans('labels.id')],
                ['data' => 'name', 'name' => 'name', 'title' => trans('labels.name')],
                ['data' => 'parent_id', 'name' => 'parent_id', 'title' => trans('labels.parent')],
                ['data' => 'status', 'name' => 'status', 'title' => trans('labels.status')],
                ['data' => 'position', 'name' => 'position', 'title' => trans('labels.position')],
	            ['data' => 'actions', 'name' => 'actions', 'title' => trans('labels.actions')],
            ])
			->parameters([
				'sPaginationType' => "bootstrap_alt",
				'drawCallback' => 'function() { initToggles(); }',
				/*'language' => ["url" => '//cdn.datatables.net/plug-ins/1.10.20/i18n/'.config('laravellocalization.supportedLocales.'.$this->locale.'.name','Russian').'.json'],*/
			]);

        $this->fillAdditionTemplateData();

		$categories = [];
        $categories_list = Category::withDepth()->get()->toFlatTree();

        foreach($categories_list as $item) $categories[$item->id] = substr('-------------------------------------------',0,$item->depth).' '.$item->name;

        $this->data('categories', $categories);
        $this->data('selectedParentCategory',$selectedParentCategory);

        return $this->render('views.catalog.category.index')
			->with('html',$html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $model = new Category;
        $this->data('model', $model);

        $this->fillAdditionTemplateData();

        return $this->render('views.catalog.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryCreateRequest $request,ImageUploader $ImageUploader)
    {

        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;

        $model = new Category($input);

        if ($model->save()) {
			$model->parent_id =  $input['parent_id'];
			$model->save();

            if (request()->hasFile('imageUpload')) {
                try {
                    $model->image = $ImageUploader->upload(request()->file('imageUpload'), 'category', $model->id);
                    $model->save();
                } catch (Exception $e) {
                    flash( trans('messages.updating_error').': '.$e->getMessage())->error();
                }
            }

            //Event::fire(new CategoryEdit($model));

            flash(trans('messages.save_ok'))->success();

            return redirect()->route('admin.catalog.category.index');
        }

        flash( trans('messages.saving_failed'))->error();

        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        try {
            $model = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            flash( trans('messages.record_not_found'))->error();

            return redirect()->route('admin.catalog.category.index');
        }

        $this->breadcrumbs($model->name);

        $this->fillAdditionTemplateData($model);

        return $this->render('views.catalog.category.edit')->with('model',$model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id, CategoryUpdateRequest $request,ImageUploader $ImageUploader)
    {
        try {
            $model = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            flash( trans('messages.record_not_found'))->error();

            return redirect()->route('admin.catalog.category.index');
        }

        $input = $request->all();
        $input['parent_id'] = isset($input['parent_id']) ? $input['parent_id'] : null;

        $model->fill($input);

        if (request()->hasFile('imageUpload')) {
            try {
                $model->image = $ImageUploader->upload(request()->file('imageUpload'), 'category', $model->id);
            } catch (Exception $e) {
                flash( trans('messages.image_save_error').': '.$e->getMessage())->error();
            }
        }
        try {
            $model->save();

            //Event::fire(new CategoryEdit($model));

            flash( trans('messages.update_ok'))->success();

            return redirect()->route('admin.catalog.category.index');
        } catch (Exception $e) {
            flash( trans('messages.saving_failed').': '.$e->getMessage())->error();

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $model = Category::findOrFail($id);

            $image = public_path($model->image);

            $model->delete();
          //  $model->products()->sync($id);

            DB::commit();

            unset($image);

            //Event::fire(new CategoryEdit());

            flash( trans('messages.destroy_ok'))->success();

            return redirect()->route('admin.catalog.category.index');
        } catch (ModelNotFoundException $e) {
            flash( trans('messages.record_not_found'))->error();
        } catch(Exception $e) {
            flash( trans('messages.destroy_error') . ': ' . $e->getMessage())->error();
        }

        DB::rollBack();

        return redirect()->route('admin.catalog.category.index');
    }

    /**
     * @param null $model
     */
    public function fillAdditionTemplateData($model = null)
    {
        //select categories
        $list = Category::with('translations')->withDepth()->get()->toFlatTree();

        $parents = ['' => trans('labels.not_set')];
        foreach ($list as $item) {
			if($model && $item->id == $model->id) { ; }
			else { $parents[$item->id] = substr('-------------------------------------------',0,$item->depth).''.$item->name; }
        }
        $this->data('parents', $parents);
    }

    public function ajaxFindCategories() {
        $result = [];
        $categories = [];
        $string = request()->get('value');
        $class = request()->get('_class');
        $type = request()->get('type','related');
                if ($string) {
                    $categories = \DB::table($class . '_translations');
                    if(strpos($string,'#') === FALSE) {
                        $categories = $categories->where('name', 'LIKE','%'. $string .'%');
                    }
                    else
                    {
                        $categories = $categories->where($class . '_id', explode('#',$string)[1]);
                    }
                    $categories = $categories->select('name as name', $class .'_id as id')->get();
            }
        foreach ($categories as $category) {
            $result[$category->id] = \View::make('views.product.partials.matches')->with(['data' => $category, 'class' => $class, 'type' => $type])->render();
        }

        return ['status' => 'success', 'data' => $result];
    }

    public function saveCategoryPosition(){
        $data = request()->get('items');
        /*$array = json_decode($data);*/
        foreach ($data as $key => $value){
            Category::find($value)->update([ 'position' => $key ]);
            /*echo $key.' '.Category::find($value)->position.' // ';*/
        }
        return response()->json(
            ["error" => 0, 'message' => trans('messages.position_successfully_saved'), 'type' => 'success']
        );
    }

	public function postStatus($id)
	{
		 $data = request()->only('status');
		 Category::find($id)->update($data);
		return response()->json(["error"=>0]);
	}

}
