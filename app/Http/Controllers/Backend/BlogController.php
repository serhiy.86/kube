<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Blog\BlogCreateRequest;
use App\Http\Requests\Backend\Blog\BlogUpdateRequest;
use App\Models\Blog;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Models\BlogCategory;
use DataTables;
use DB;
use Exception;
//use FlashMessages;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Redirect;
use Response;

/**
 * Class BlogController
 * @package App\Http\Controllers\Backend
 */
class BlogController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "blog";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'blog.read',
        'create'          => 'blog.create',
        'store'           => 'blog.create',
        'show'            => 'blog.read',
        'edit'            => 'blog.read',
        'update'          => 'blog.write',
        'destroy'         => 'blog.delete',
        'ajaxFieldChange' => 'blog.write',
    ];

    /**
     * Display a listing of the resource.
     * GET /Blog
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Blog::withTranslations()->joinTranslations('blog', 'blog_translations')->select(
                    'blog.id',
                    'blog_translations.title',
                    'blog.created_at',
                    'blog.created_by',
                    'blog.modified_by',
                    'status'
                );
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'title',
                    function ($query, $keyword) {
                        $query->whereRaw("blog_translations.title like ?", ["%{$keyword}%"]);
                    }
                )
                ->editColumn(
                    'created_by',
                    function ($model) {
                        return $model->creator ? $model->creator->email : '';
                    }
                )
                ->editColumn(
                    'modified_by',
                    function ($model) {
                        return $model->moderator ? $model->moderator->email : '';
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'actions'])
                ->make();

        }

        $this->data('page_title', trans('labels.blog'));
        $this->breadcrumbs(trans('labels.list'));

        return $this->render('views.'.$this->module.'.index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /blog/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new Blog());

        $this->data('page_title', trans('labels.creating'));

        $this->breadcrumbs(trans('labels.blog'));

        $this->_fillAdditionTemplateData();

        return $this->render('views.'.$this->module.'.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /Blog
     *
     * @param BlogCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BlogCreateRequest $request)
    {
        DB::beginTransaction();

        try {

            $input = $request->all();
            $blog_categories = $request->get('blog_categories');

            $model = new Blog($input);
            $model->status = $input['status'];
            $model->created_by = $this->user->id;
            $model->save();

            if($blog_categories) {
                $model->categories()->sync($blog_categories);
            }

            DB::commit();

//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add('error', trans('messages.save_failed'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * GET /blog/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /blog/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        try {
            $model = Blog::findOrFail($id);

            $this->data('page_title', trans('labels.editing'));

            $this->breadcrumbs(trans('labels.blog'));

            $this->_fillAdditionTemplateData();

            return $this->render('views.'.$this->module.'.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /blog/{id}
     *
     * @param  int           $id
     * @param BlogCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, BlogUpdateRequest $request)
    {
        try {
            $input = $request->all();
            $blog_categories = $request->get('blog_categories');
            $model = Blog::findOrFail($id);

            $model->fill($input);
            $model->status = $input['status'];
            $model->modified_by = $this->user->id;
            $model->save();

            if($blog_categories){
                $model->categories()->sync($blog_categories);
            }  else {
                $model->categories()->sync(array());
            }

//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add("error", trans('messages.update_error'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /blog/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $model = Blog::findOrFail($id);

            if (!$model->delete()) {
//                FlashMessages::add("error", trans("messages.destroy_error"));
            } else {
//                FlashMessages::add('success', trans("messages.destroy_ok"));
            }
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
//            FlashMessages::add("error", trans('messages.delete_error'));
        }

        return redirect()->route('admin.'.$this->module.'.index');
    }

    private function _fillAdditionTemplateData()
    {
        $blog_categories = BlogCategory::withTranslations()->joinTranslations('blog_categories', 'blog_category_translations', 'id', 'blog_category_id')->pluck('blog_category_translations.title', 'blog_categories.id')->toArray();
        $this->data('blog_categories', $blog_categories);
    }
}
