<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\URL as URLHelper;

if (!function_exists('ed')) {
    /**
     * Dump the passed variables.
     *
     * @param mixed
     *
     * @return void
     */
    function ed(...$args)
    {
        foreach ($args as $x) {
            (new Illuminate\Support\Debug\Dumper)->dump($x);
        }
    }
}

/**
 * Show last query to database
 */
if (!function_exists('get_last_query')) {
    /**
     * @return mixed
     */
    function get_last_query()
    {
        $queries = \DB::getQueryLog();

        $sql = end($queries);

        if (!empty($sql['bindings'])) {
            $pdo = \DB::getPdo();

            foreach ($sql['bindings'] as $binding) {
                $sql['query'] =
                    preg_replace(
                        '/\?/',
                        $pdo->quote($binding),
                        $sql['query'],
                        1
                    );
            }
        }

        return $sql['query'];
    }
}


if (!function_exists('route_is')) {
    /**
     * @param string $pattern
     *
     * @return boolean
     */
    function route_is($pattern)
    {
        return (preg_match("/$pattern/", \Route::currentRouteName())) ? true : false;
    }
}

if (!function_exists('check_local')) {
    /**
     * @param null $url
     *
     * @return bool
     */
    function check_local($url = null)
    {
        $url = $url ? : Request::root();

        return get_url_host($url) == get_url_host(config('app.url'));
    }
}

if (!function_exists('get_url_host')) {
    /**
     * @param string $url
     *
     * @return string
     */
    function get_url_host($url)
    {
        preg_match('/^((http[s]?|ftp):\/\/)?(www\.)?([\w\-\.]+)(\/)?(.*)$/i', $url, $host);

        if (!empty($host)) {
            return !empty($host[4]) ? $host[4] : $url;
        }

        return $url;
    }
}


if (!function_exists('get_class_name_from_namespace')) {
    /**
     * @param string|Object $object
     *
     * @return string
     */
    function get_class_name_from_namespace($object)
    {
        if (is_object($object)) {
            $object = class_basename($object);
        }

        $object = explode('\\', $object);

        return array_pop($object);
    }
}


if (!function_exists('variable')) {
    /**
     * Get / set the specified variable value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param array|string $key
     * @param mixed        $default
     *
     * @return mixed
     */
    function variable($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('variable');
        }

        return app('variable')->get($key, $default);
    }
}


if (!function_exists('random_string')) {
    /**
     * Create a Random String
     *
     * Useful for generating passwords or hashes.
     *
     * @access    public
     *
     * @param integer $length number of characters
     * @param string  $type // of random string.  basic, alpha, all, numeric, no_zero, unique, md5, encrypt and sha1
     *
     * @return    string
     */
    function random_string($length = 8, $type = 'all')
    {
        switch ($type) {
            case 'basic':
                return mt_rand();
                break;

            case 'all':
            case 'numeric':
            case 'no_zero':
            case 'alpha':
            case 'alpha_num':
            case 'lover_alpha_num':
                switch ($type) {
                    case 'lover_alpha_num':
                        $pool = '0123456789abcdefghijklmnopqrstuvwxyz';
                        break;
                    case 'alpha':
                        $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'alpha_num':
                        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                        break;
                    case 'numeric':
                        $pool = '0123456789';
                        break;
                    case 'no_zero':
                        $pool = '123456789';
                        break;
                    default:
                        $pool = '!@#$%^&*()_+/|\?.,><~`=-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                }

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }

                return $str;

                break;

            case 'unique':
            case 'md5':
                return md5(uniqid(mt_rand()));

                break;

            case 'encrypt':
            case 'sha1':
                return hash('sha1', uniqid(mt_rand(), true));

                break;
        }
    }
}

if (!function_exists('array_to_str')) {
    /**
     * @param array      $array
     * @param string|int $parent_key
     *
     * @return string
     */
    function array_to_str($array, $parent_key = '')
    {
        if (!is_array($array)) {
            return $array;
        }

        $str = '';

        foreach ($array as $key => $item) {
            $str .= ($parent_key ? $parent_key.'.'.$key : $key).' = '.
                (
                !is_array($item) ?
                    '"'.($item).'"; ' :
                    array_to_str($item, $parent_key ? $parent_key.'.'.$key : $key)
                );
        }

        return $str;
    }
}

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = '';

        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));

                $headers[$key] = $value;
            }
        }

        return $headers;
    }
}

if (!function_exists('localize_url')) {
    /**
     * @param null|string $url
     * @param null|string $locale
     *
     * @return string
     */
    function localize_url($url = null, $locale = null)
    {
        $locale = $locale ? : app()->getLocale();
        $url = $url ? ($url == '/' ? route('admin.home') : $url) : URL::full();

        return check_local($url) ? LaravelLocalization::getLocalizedURL($locale, $url) : $url;
    }
}


if (!function_exists('active_class')) {
    /**
     * @param              $pattern
     * @param string       $class
     * @param string|array $exclude
     *
     * @return string
     */
    function active_class($pattern, $class = 'active', $exclude = '')
    {
        $pattern = str_replace('.', '\.', $pattern);

        if (preg_match('/^\*.*\*$/', $pattern)) {
            $pattern = str_replace('*', '.*', $pattern);

            $result = preg_match('/'.$pattern.'/', URLHelper::full()) ? true : false;
        } elseif (preg_match('/.*\*$/', $pattern)) {
            $pattern = str_replace('*', '', $pattern);

            $result = route_is("^$pattern") ? true : false;
        } else {
            $result = route_is("^$pattern$") ? true : false;
        }

        $_result = false;

        if ($exclude) {
            foreach ((array) $exclude as $_pattern) {
                $_pattern = str_replace('.', '\.', $_pattern);
                if (preg_match('/^\*.*\*$/', $pattern)) {
                    $pattern = str_replace('*', '.*', $pattern);

                    $_result = preg_match('/'.$pattern.'/', URLHelper::full()) ? true : false;
                } elseif (preg_match('/.*\*$/', $pattern)) {
                    $_pattern = str_replace('*', '', $_pattern);

                    $_result = route_is("^$_pattern") ? true : false;
                } else {
                    $_result = route_is("^$_pattern$") ? true : false;
                }

                if ($_result) {
                    break;
                }
            }
        }

        return $result && !$_result ? $class : '';
    }
}


if (!function_exists('bindedQuery')) {
    /**
     * @param      $query
     * @param null $bindings
     *
     * @return string
     */
    function bindedQuery($query, $bindings = null)
    {
        $sql = $query instanceof Builder ? $query->toSql() : $query;

        $sql = str_replace(
            '?',
            '%s',
            str_replace(
                "\n",
                ' ',
                preg_replace(
                    '!\s+!',
                    ' ',
                    $sql
                )
            )
        );

        $handledBindings = array_map(
            function ($binding) {
                if (is_numeric($binding)) {
                    return $binding;
                }

                if (is_bool($binding)) {
                    return ($binding) ? 'true' : 'false';
                }

                return "'{$binding}'";
            },
            empty($bindings) ? ($query instanceof Builder ? $query->getBindings() : []) : $bindings
        );

        return vsprintf($sql, $handledBindings);
    }
}

if (!function_exists('noImage')) {
    /**
     * @param int      $width
     * @param int|null $height
     *
     * @return string
     */
    function noImage($width, $height = null): string
    {
        $height = is_null($height) ? $width : $height;

        return 'https://www.placehold.it/'.$width.'x'.$height.'/EFEFEF/AAAAAA&text=no+image';
    }
}

if (!function_exists('get_hashed_url')) {
    /**
     * @param        $model
     * @param string $type
     * @param string $key_field
     *
     * @return string
     */
    function get_hashed_url($model, $type = 'page', $key_field = 'slug')
    {
        return md5($type.'_'.$model->id.'_'.$model->{$key_field});
    }
}

if (!function_exists('str_limit')) {
    /**
     *
     * @return string
     */
    function str_limit(string $value, int $limit = 100, string $end = '...'): string
    {
		return \Illuminate\Support\Str::limit($value,$limit,$end);
	}
}

if (!function_exists('studly_camel_case')) {
    /**
     * @param string
     *
     * @return string
     */
    function studly_camel_case($string)
    {
        return studly_case(camel_case($string));
    }

if (!function_exists('thumb')) {
    /**
     * @param string   $path
     * @param int      $width
     * @param int|null $height
     *
     * @return string
     *
     */
    function thumb($path = '', $width = null, $height = null)
    {
        $thumb = null;

        if (URL::isValidUrl($path)) {
            return $path;
        }

        $height = $height ? : $width;
        $path = File::exists(public_path($path)) ? $path : null;

        if ($path) {
            if (!$width) {
                $img_info = getimagesize(public_path($path));

                $width = $img_info[0];
                $height = $img_info[1];
            } elseif ($width && $height) {
                $img_info = getimagesize(public_path($path));

                if (!empty($img_info)) {
                    $width = $width <= $img_info[0] ? $width : $img_info[0];
                    $height = $height <= $img_info[1] ? $height : $img_info[1];
                }
            }

            $thumb = url(Thumb::thumb(public_path($path), $width, $height)->link());
        }

        return $thumb ? : asset('assets/themes/admin/images/no_image.png');
    }


  }
}
if (!function_exists('get_model_by_controller')) {
    /**
     * @param $class
     *
     * @return string
     */
    function get_model_by_controller($class)
    {
        $class = explode('\\', str_replace('Controller', '', $class));

        return array_pop($class);
    }
}
