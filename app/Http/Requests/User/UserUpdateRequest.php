<?php

namespace App\Http\Requests\User;

use App\Models\User;
use App\Http\Requests\BaseRequest;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\Backend\User
 */
class UserUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('user');

        $rules = [
//            'email'      => 'required|email|unique:users,email,' . $id,
//            'phone'       => 'nullable|phone',
            'first_name' => 'nullable|string|max:255',
            'last_name'  => 'nullable|string|max:255',
//            'gender'      => 'nullable|integer|in:'.implode(',', array_keys(app(User::class)->getGenders())),
//            'address'     => 'nullable|string|min:2',
//            'city'        => 'nullable|string|min:2',
//            'postal_code' => 'nullable|string',
//            'country_id'  => 'nullable|integer|exists:countries,id',
//            'roles'       => 'array',
        ];

        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'user_name.regex' => trans('validation.user_name_regex')
        ];
    }
}
