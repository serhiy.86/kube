<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\AuthService;
use Illuminate\Http\Request;

class FrontendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $auth;

    public $_data=[];

    public $layout = 'app';

    public $module = '';

    public function __construct(AuthService $auth, Request $request)
    {
        $this->auth = $auth;

        //parent::__construct();
    }

    public function view($template_path,array $compactdata=[])
    {
        $currentUser = $this->auth->getUser();

        $this->data('currentUser',$currentUser);
        return view('views.' . implode('.', array_filter([$this->module, $this->layout , $template_path]) ),$compactdata)->with($this->_data);
    }

    public function render($template_path,array $compactdata=[])
    {
        $currentUser = $this->auth->getUser();
        if($currentUser && $this->usertype) { $currentUser = $currentUser->{$this->usertype}; }
        $this->data('currentUser',$currentUser);
        return view('views.' . $template_path,$compactdata)->with($this->_data);
    }

    public function data($key,$value=NULL)
    {
	$this->_data[$key] = $value;
    }

    public function sendResponse($result, $message)
    {
        return response()->json($message, $result);
    }
}
