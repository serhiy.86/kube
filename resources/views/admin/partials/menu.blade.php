<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            @if ($user->hasAccess(['role.read', 'user.read']))
                <li class="header">@lang('labels.users')</li>
            @endif

            @if ($user->hasAccess('user.read'))
                <li class="{!! active_class('admin.user.index*') !!}">
                    <a href="{!! route('admin.user.index') !!}">
                        <i class="fa fa-user"></i>
                        <span>@lang('labels.users')</span>

                        @if ($user->hasAccess('user.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_user')"
                                   data-href="{!! route('admin.user.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif
            @if ($user->hasAccess('speaker.read'))
                <li class="{!! active_class('admin.speaker.index*') !!}">
                    <a href="{!! route('admin.speaker.index') !!}">
                        <i class="fa fa-user"></i>
                        <span>@lang('labels.speakers')</span>

                        @if ($user->hasAccess('speaker.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_speaker')"
                                   data-href="{!! route('admin.speaker.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif
            @if ($user->hasAccess('role.read'))
                <li class="{!! active_class('admin.role.index*') !!}">
                    <a href="{!! route('admin.role.index') !!}">
                        <i class="fa fa-users"></i>
                        <span>@lang('labels.roles')</span>

                        @if ($user->hasAccess('role.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_group')"
                                   data-href="{!! route('admin.role.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif

		@if ($user->hasAccess(['category.read','product.read']))
			<li class="header">@lang('labels.product')</li>
			@endif
      @if ($user->hasAccess('category.read'))
          <li class="{!! active_class('admin.catalog.category*') !!}">
              <a href="{!! route('admin.catalog.category.index') !!}">
                  <i class="fa fa-file-text"></i>
                  <span>@lang('labels.category')</span>

                  @if ($user->hasAccess('catalog.category.create'))
                      <small class="label create-label pull-right bg-green" title="@lang('labels.add')"
                             data-href="{!! route('admin.catalog.category.create') !!}">
                          <i class="fa fa-plus"></i>
                      </small>
                  @endif
              </a>
          </li>
      @endif

			@if ($user->hasAccess('product.read'))
                <li class="{!! active_class('admin.product*') !!}">
                    <a href="{!! route('admin.catalog.product.index') !!}">
                        <i class="fa fa-file-text"></i>
                        <span>@lang('labels.product')</span>

                        @if ($user->hasAccess('product.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add')"
                                   data-href="{!! route('admin.catalog.product.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->hasAccess(['page.read']))
                <li class="header">@lang('admin_labels.content')</li>
            @endif

           @if ($user->hasAccess('page.read'))
                <li class="{!! active_class('admin.page*') !!}">
                    <a href="{!! route('admin.page.index') !!}">
                        <i class="fa fa-file-text"></i>
                        <span>@lang('labels.pages')</span>

                        @if ($user->hasAccess('page.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_page')"
                                   data-href="{!! route('admin.page.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->hasAccess('blog.read'))
                <li class="{!! active_class('admin.blog*') !!}">
                    <a href="{!! route('admin.blog.index') !!}">
                        <i class="fa fa-book"></i>
                        <span>@lang('labels.blog')</span>

                        @if ($user->hasAccess('blog.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_blog')"
                                   data-href="{!! route('admin.blog.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif

            @if ($user->hasAccess('blog_category.read'))
                <li class="{!! active_class('admin.blog_category*') !!}">
                    <a href="{!! route('admin.blog_category.index') !!}">
                        <i class="fa fa-th-list"></i>
                        <span>@lang('labels.blog_category')</span>

                        @if ($user->hasAccess('blog_category.create'))
                            <small class="label create-label pull-right bg-green" title="@lang('labels.add_blog_category')"
                                   data-href="{!! route('admin.blog_category.create') !!}">
                                <i class="fa fa-plus"></i>
                            </small>
                        @endif
                    </a>
                </li>
            @endif
			@if ($user->hasAccess('faq_category.read'))
                    <li class="{!! active_class('admin.faq_category*') !!}">
                        <a href="{!! route('admin.faq_category.index') !!}">
                            <i class="fa fa-th-list"></i>
                            <span>@lang('labels.faq_category')</span>

                            @if ($user->hasAccess('faq_category.create'))
                                <small class="label create-label pull-right bg-green" title="@lang('labels.add_faq_category')"
                                       data-href="{!! route('admin.faq_category.create') !!}">
                                    <i class="fa fa-plus"></i>
                                </small>
                            @endif
                        </a>
                    </li>
                @endif

                @if ($user->hasAccess('faq.read'))
                    <li class="{!! active_class('admin.faq*') !!}">
                        <a href="{!! route('admin.faq.index') !!}">
                            <i class="fa fa-question-circle"></i>
                            <span>@lang('labels.faq')</span>

                            @if ($user->hasAccess('faq.create'))
                                <small class="label create-label pull-right bg-green" title="@lang('labels.add_faq')"
                                       data-href="{!! route('admin.faq.create') !!}">
                                    <i class="fa fa-plus"></i>
                                </small>
                            @endif
                        </a>
                    </li>
                @endif


				@if ($user->hasAccess('news.read'))
                    <li class="{!! active_class('admin.news*') !!}">
                        <a href="{!! route('admin.news.index') !!}">
                            <i class="fa fa-newspaper-o"></i>
                            <span>@lang('labels.news')</span>

                            @if ($user->hasAccess('news.create'))
                                <small class="label create-label pull-right bg-green" title="@lang('labels.add')"
                                       data-href="{!! route('admin.news.create') !!}">
                                    <i class="fa fa-plus"></i>
                                </small>
                            @endif
                        </a>
                    </li>
                @endif

			@if ($user->hasAccess(['translation.read']))
                <li class="header">@lang('admin_labels.settings')</li>
            @endif

            @if ($user->hasAccess('translation.read'))
                <li class="treeview {!! active_class('admin.translation.index*') !!}">
                    <a href="#">
                        <i class="fa fa-language"></i>
                        <span>@lang('admin_labels.translations')</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($translation_groups as $group)
                            <li class="{!! url_active_class('') !!}">
                                <a href="">
                                    <span>@lang('admin_labels.translation_group_' . $group)</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif

				@if ($user->hasAccess(['variable.value.read']))
                    <li class="header">@lang('labels.directory')</li>
                @endif

                 @if ($user->hasAccess('variable.show.content'))
                <li class="{!! active_class('admin.variable*') !!}">
                    <a href="{!! route('admin.variable.index') !!}">
                        <i class="fa fa-cog"></i>
                        <span>@lang('labels.variables')</span>
                    </a>
                </li>
            	@endif
        </ul>
    </section>
</aside>
