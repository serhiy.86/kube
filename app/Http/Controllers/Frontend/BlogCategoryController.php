<?php namespace App\Http\Controllers\Frontend;

use App\Models\BlogCategory;
use App\Models\Blog;
use App\Api\Transformers\BlogCategoryTransformer;

use App\Http\Controllers\ApiController;


class BlogCategoryController extends ApiController
{
    /**
     * Eloquent model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function model()
    {
        return new BlogCategory;
    }

    /**
     * Transformer for the current model.
     *
     * @return \League\Fractal\TransformerAbstract
     */
    protected function transformer()
    {
        return new BlogCategoryTransformer;
    }

    public function index($id=NULL)
    {
        $items = BogCategory::positionSorted()->visible()->get();

        return $this->respondWithCollection($items);
    }
}
