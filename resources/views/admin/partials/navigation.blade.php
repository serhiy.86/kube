<header class="main-header">
    <div class="logo position-relative">
        <a href="{!! route('admin.home') !!}" class="logo-link">
            <span class="logo-mini upper-case">{!! str_limit(config('app.name'), 3, '') !!}</span>

            <span class="logo-lg upper-case">
                {!! str_limit(config('app.name'), 12) !!}
            </span>
        </a>

        <div class="front-home-link" data-href="/" title="@lang('admin_labels.go_to_front')">
            <i class="fa fa-external-link"></i>
        </div>
    </div>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">@lang('admin_labels.toggle_navigation')</span>
        </a>
    </nav>
</header>

