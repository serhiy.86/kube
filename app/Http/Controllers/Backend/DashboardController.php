<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Services\AuthService;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends BackendController
{

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {

        return $this->render('views.dashboard');
    }

}