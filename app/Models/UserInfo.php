<?php namespace App\Models;

use Eloquent;

class UserInfo extends Eloquent
{
    public $timestamps = false;

    protected $table = 'user_info';

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'phone',
        'gender',
        'birthday',
        'avatar',
		'country',
		'type',
		'investments'
    ];

    /**
     * @var array
     */
    public static $genders = ['male', 'female'];
    
    public function user()
    {

        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
