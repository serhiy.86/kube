<?php namespace App\Models;

use Eloquent;

class UserSetting extends Eloquent
{
    public $timestamps = false;

    protected $table = 'user_setting';

    protected $fillable = [
        'user_id',
        'locale'
    ];

    public function user()
    {

        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
