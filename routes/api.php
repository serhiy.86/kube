<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix'=>'v1'], function () {

    Route::post('login', 'AuthController@login')->name('login');
	Route::post('logout', 'AuthController@logout')->name('logout');

    Route::group(['prefix'=>'oauth'], function() {
		Route::get('/{provider}/login', 'SocialAuthController@handleToken');
		Route::post('/{provider}/login', 'SocialAuthController@handleToken');

		Route::get('/{provider}/link', 'SocialAuthController@linkToken');
		Route::post('/{provider}/link', 'SocialAuthController@linkToken');

		Route::post('/{provider}/unlink', 'SocialAuthController@unlinkToken');

    });
    Route::group(['prefix'=>'oauth'], function() {
        Route::get('/{provider}', 'SocialAuthController@redirectToProvider')->name('auth.social.login');
        Route::get('/{provider}/callback', 'SocialAuthController@handleProviderCallback')->name('auth.social.callback');
    });

	Route::group(['middleware' => ['jwt.auth']], function() {
		Route::get('me', 'UserController@me');

		Route::post('account/change_password', 'UserController@changePassword');
        Route::post('account/update', 'UserController@updateMyInfo');
		Route::post('account/avatar', 'UserController@updateMyPhoto');
        Route::get('account/delete', 'UserController@delete');

		Route::post('logout', 'AuthController@logout');
	});

	Route::post('refresh', 'AuthController@refresh');

	Route::post('register', 'AuthController@register');
	Route::post('register_complete', 'AuthController@register_complete');

    Route::post('activation', 'AuthController@activation');
	Route::post('activation/resend', 'AuthController@reactivation');
});
