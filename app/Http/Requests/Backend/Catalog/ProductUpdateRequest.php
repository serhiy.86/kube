<?php

namespace App\Http\Requests\Backend\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
  public function rules()
  {
      $rules = [
          'status'        => 'required|boolean',
         // 'slug'     => 'unique:products,slug',
        ];

      $languageRules = [
          'title' => 'required|string',
          'content' => 'required|string',
          'short_content' => 'nullable|string',
          'h1' => 'nullable|string',
          'meta_title' => 'nullable|string',
          'meta_description' => 'nullable|string',
          'meta_keyword' => 'nullable|string',
      ];

      foreach (config('app.locales') as $locale) {
          foreach ($languageRules as $name => $rule) {
              $rules[$locale.'.'.$name] = sprintf($rule, $locale);
          }
      }

      return $rules;
  }
}
