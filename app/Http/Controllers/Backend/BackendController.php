<?php

namespace App\Http\Controllers\Backend;

use App\Services\UserService;
use Module\Role\Contract\Permissions as PermissionsContract;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use JavaScript;
use Theme;
use App\Services\AuthService;
use App;
use Illuminate\Support\Arr;

class BackendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $auth;

	/**
     * @var array
     */
    public $viewData = [];

    /**
     * @var string
     */
    public $_view = 'home';

    /**
     * @var string
     */
    public $_theme = 'admin';

    /**
     * @var array
     */
    public $breadcrumbs = [];

    /**
     * @var null
     */
    public $messageBag = null;


    /**
     * @var \App\Contracts\Permissions|PermissionsContract
     */
    protected $permissions;


    /**
     * Карта доступа к методам.
     * Пара метод => права доступа
     * Ключ all - права доступа ков сем методам класса
     */
    public $accessMap = [];

    /**
     * @var \App\Services\UserService
     */
    protected $userService;


    public function __construct(AuthService $auth, PermissionsContract $permissions, UserService $userService)
    {
        $this->auth = $auth;

		$this->permissions = $permissions;

        $this->userService = $userService;

		$this->middleware(
            function ($request, $next) {
                view()->share('user', $this->auth->getUser());

				$this->locale = App::getLocale();
				$this->user = $this->auth->getUser();

                return $next($request);
            }
        );

		Theme::set($this->_theme);

        //parent::__construct();
    }

	/**
     * @param string $view
     * @param array  $data
     *
     * @return \Illuminate\Contracts\View\View|string
     * @throws \Throwable
     */
    public function render($view = '', array $data = [])
    {

		$this->data('breadcrumbs', $this->breadcrumbs);

        $this->fillThemeData();

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->data($k, $v);
            }
        }


        if (view()->exists($view)) {
            return view($view, $this->viewData)
				->with($data)
				//->with('messages', FlashMessages::retrieve())
				//->render()
				;
        }


        throw new \Exception('View not found', 500);
    }

    /**
     * Push data to templates
     *
     * @return bool
     */
    public function data( /*array or pair of values*/)
    {
        $data = func_get_args();

        if (!empty($data)) {
            if (count($data) > 1) {
                $this->viewData[$data[0]] = $data[1];
            } elseif (is_array($data[0])) {
                $this->viewData = array_merge($this->viewData, $data[0]);
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $name
     * @param bool   $url
     */
    public function breadcrumbs($name, $url = false)
    {
        $this->breadcrumbs[] = ['name' => $name, 'url' => $url];
    }

	 /**
     * Call controller with the specified parameters.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return bool|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
	/*
    public function callAction($method, $parameters)
    {
        if (!empty($this->user)) {
            $permission = Arr::get($this->accessMap, $method, false);
            // если нет соотв. прав - проверим, может заданы права к абсолютно всем методам
            if (!$permission) {
                $permission = Arr::get($this->accessMap, 'all', false);
            }

            if ($permission) {

                $protect = $this->protect($permission);

                if ($protect !== true) {
                    return $protect;
                }
            }
        }

        return parent::callAction($method, $parameters);
    }
	*/

    /**
     * @param $permission
     *
     * @return bool
     */
    public function isActionAllowed($permission)
    {
        return $this->user->hasAccess($permission);
    }

    /**
     * @param      $permission
     * @param bool $verbose
     *
     * @return bool|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function protect($permission, $verbose = true)
    {
        if (!$this->isActionAllowed($permission)) {
            $message = trans('admin_messages.access not allowed');

            if (Request::ajax()) {
                return Response::json(['message' => $message, 'status' => 'warning']);
            } else {
                if ($verbose) {
                    FlashMessages::add('warning', $message);

                    return redirect()->route('admin.home');
                }

                return false;
            }
        }

        return true;
    }


    /**
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile()
    {
        $file = request('file', null);

        if ($file && File::exists(public_path($file))) {
            return response()->download(public_path($file));
        }

        if (URL::isValidUrl($file)) {
            return redirect($file);
        }

        return redirect()->back();
    }

    /**
     * fill theme data
     */
    private function fillThemeData()
    {
        //Meta::title(config('app.name'));

        view()->share('lang', app()->getLocale());
        view()->share('locales', config('app.admin_locales'));

        view()->share('elfinder_link_name', 'link');

        $upload_max_filesize = (int) ini_get("upload_max_filesize") * 1024 * 1024;
        view()->share('upload_max_filesize', $upload_max_filesize);

        $no_image = 'https://www.placehold.it/250x250/EFEFEF/AAAAAA&text=no+image';
        view()->share('no_image', $no_image);

        JavaScript::put(
            [
                'no_image'                         => $no_image,
                'select2_per_page'                 => 15,
                'lang'                             => app()->getLocale(),
                'lang_yes'                         => trans('admin_labels.yes'),
                'lang_save'                        => trans('admin_labels.save'),
                'lang_cancel'                      => trans('admin_labels.cancel'),
                'lang_close'                       => trans('admin_labels.close'),
                'lang_error'                       => trans('admin_messages.an error has occurred, please reload the page and try again'),
                'lang_errorSelectedFileIsTooLarge' => trans('admin_messages.trying to load is too large file'),
                'lang_errorIncorrectFileType'      => trans('admin_messages.trying to load unsupported file type'),
                'lang_errorFormSubmit'             => trans('admin_messages.error form submit'),
                'lang_errorValidation'             => trans('admin_messages.validation_failed'),
                'lang_errorEmptyData'              => trans('admin_messages.you have not entered any data'),
                'lang_errorEmptyNameField'         => trans('admin_messages.name field not set'),
                'lang_warningEndOfTable'           => trans('admin_messages.end of table reached'),
                'upload_max_filesize'              => $upload_max_filesize,
                'elfinderConnectorUrl'             => route('admin.elfinder.connector'),
                'birthday_format'                  => 'dd.mm.yyyy',
            ]
        );

        view()->share('translation_groups', config('translation.visible_groups',[]));
    }
}
