<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'           => 'Ці дані не відповідають нашим записам.',
    'password'         => 'Введений пароль неправильний.',
    'throttle'         => 'Забагато спроб входу. Повторіть спробу через: секунди секунд.',
    'activation_email' => 'Активація пошти',
    'welcome_email'    => 'Вітальний лист',
    'reminder_email'   => 'Зміна пароля',

];
