<div class="row">
    <div class="col-md-3">
        <div class="box-body box-profile">
            @include('partials.tabs.user_avatar')

            <h3 class="profile-username text-center">{!! $model->getFullName() !!}</h3>

            @if (!empty($model->roles()->first()))
                <p class="text-muted text-center">{!! $model->roles()->first()->name !!} </p>
            @endif
        </div>
    </div>

    <div class="col-md-9">
        <div class="box-body">
            <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                {!! Form::label('first_name', trans('labels.first_name'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-5">
                    {!! Form::text('first_name', null, ['placeholder' => trans('labels.first_name'), 'class' => 'form-control input-sm']) !!}

                    {!! $errors->first('first_name', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                {!! Form::label('last_name', trans('labels.last_name'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-5">
                    {!! Form::text('last_name', null, ['placeholder' => trans('labels.last_name'), 'class' => 'form-control input-sm']) !!}

                    {!! $errors->first('last_name', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group required @if ($errors->has('email')) has-error @endif">
                {!! Form::label('email', trans('labels.email'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::text('email', null, ['placeholder' => trans('labels.email'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                    {!! $errors->first('email', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group @if ($errors->has('phone')) has-error @endif">
                {!! Form::label('phone', trans('labels.phone'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-md-3">
                    {!! Form::text('phone', null, ['placeholder' => trans('labels.phone'), 'class' => 'form-control input-sm inputmask-2']) !!}

                    {!! $errors->first('phone', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            @if(empty($model->id))
                <div class="form-group @if ($errors->has('password')) has-error @endif">
                    {!! Form::label('password', trans('labels.password'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-md-3">
                        {!! Form::text('password', null, ['placeholder' => trans('labels.password'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                        {!! $errors->first('password', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
                    {!! Form::label('password_confirmation', trans('labels.password_confirmation'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-md-3">
                        {!! Form::text('password_confirmation', null, ['placeholder' => trans('labels.password_confirmation'), 'required' => true, 'class' => 'form-control input-sm']) !!}

                        {!! $errors->first('password_confirmation', '<p class="help-block error">:message</p>') !!}
                    </div>
                </div>
            @endif


            <div class="form-group @if ($errors->has('gender')) has-error @endif">
                {!! Form::label('gender', trans('labels.gender'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-xs-12 col-sm-2 col-md-2">
                    {!! Form::select('gender', ['male' => trans('labels.gender_male'), 'female' => trans('labels.gender_female')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true']) !!}

                    {!! $errors->first('gender', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            <div class="form-group required @if ($errors->has('activated')) has-error @endif">
                {!! Form::label('activated', trans('labels.activated'), ['class' => 'col-md-3 control-label']) !!}

                <div class="col-xs-12 col-sm-2 col-md-2">
                    {!! Form::select('activated', ['0' => trans('labels.no'), '1' => trans('labels.yes')], null, ['class' => 'form-control select2 input-sm', 'aria-hidden' => 'true', 'required' => true]) !!}

                    {!! $errors->first('activated', '<p class="help-block error">:message</p>') !!}
                </div>
            </div>

            @if($model->enable_2_fact)
                <div>
                    {!! Form::label('enable_2_fact', trans('labels.two_factor_authentication'), ['class' => 'col-md-3 control-label']) !!}

                    <div class="col-xs-12 col-sm-2 col-md-2">
                        {!! Form::checkbox('enable_2_fact', 1, $model->enable_2_fact, ['class' => 'square', 'aria-hidden' => 'true']) !!}
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
