<?php

namespace App\Models\Traits;

/**
 * Trait ExtendedMutator
 * @package App\Models\Traits
 */
trait ExtendedMutator
{
    
    /**
     * @return array
     */
    public function getCastsInteger()
    {
        return isset($this->casts_integer) ? (array) $this->casts_integer : [];
    }
    
    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if (!empty($this->getCastsInteger()) && in_array($key, $this->getCastsInteger())) {
            $this->attributes[$key] = intval(cbcmul($value, 100, 0));
            
            return $this;
        }
        
        return parent::setAttribute($key, $value);
    }
    
    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getAttribute($key)
    {
        if (!empty($this->getCastsInteger()) && in_array($key, $this->getCastsInteger())) {
            return isset($this->attributes[$key])
                ? round(cbcdiv($this->attributes[$key], 100, 2), 2)
                : 0;
        }
        
        return parent::getAttribute($key);
    }
    
    /**
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();
        
        $attributes = $this->addCastsIntegerAttributesToArray($attributes);

        return $attributes;
    }
    
    /**
     * @param array $attributes
     *
     * @return array
     */
    protected function addCastsIntegerAttributesToArray(array $attributes = [])
    {
        foreach ($this->getCastsInteger() as $attribute) {
            if (!isset($attributes[$attribute])) {
                continue;
            }
            
            $attributes[$attribute] = $this->getAttribute($attribute);
        }
        
        return $attributes;
    }
}