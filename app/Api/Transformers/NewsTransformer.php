<?php namespace App\Api\Transformers;

use App\Models\News;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array.
     *
     * @param $item
     * @return array
     */
    public function transform(News $item)
    {
        return [
            'id'               => (int)$item->id,
            'slug'             => (string)$item->slug,
            'name'             => (string)$item->name,
            'image'            => (string)asset($item->image),
            'content'          => (string)$item->content,
            'short_content'    => (string)$item->content,
            'view_count'       => (string)$item->content,
            'date'             => (string)$item->date,
            'h1'               => (string)$item->h1,
            'meta_title'       => (string)$item->meta_title,
            'meta_keywords'    => (string)$item->meta_keywords,
            'meta_description' => (string)$item->meta_description,
        ];
    }
}
