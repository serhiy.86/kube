<?php namespace App\Models;

use Eloquent;

class UserSocial extends Eloquent
{

    protected $fillable = [
        'user_id',
        'provider',
        'provider_id'
    ];
    
    public function user()
    {

        return $this->belongsTo('App\Models\User', 'user_id');
    }
}
