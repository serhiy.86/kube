<?php

namespace App\Http\Requests\Backend\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use FlashMessages;
use Config;

class ProductCreateRequest extends FormRequest
{
//  protected $dontFlash = ['imageUpload'];

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
   public function rules()
   {
       $rules = [
           'status'        => 'required|boolean',
           'slug'     => 'unique:products,slug',
           'author_id'     => 'nullable|integer|min:1',
           'speaker_id'     => 'nullable|integer|min:1',
           'catalog_filter_id'     => 'nullable|integer|min:1',
           'price'     => 'nullable|integer|min:1',
           'count_students_end'     => 'nullable|integer|min:1',
         ];

       $languageRules = [
           'title' => 'required|string',
           'content' => 'required|string',
           'short_content' => 'nullable|string',
           'h1' => 'nullable|string',
           'meta_title' => 'nullable|string',
           'meta_description' => 'nullable|string',
           'meta_keyword' => 'nullable|string',
       ];

       foreach (config('app.locales') as $locale) {
           foreach ($languageRules as $name => $rule) {
               $rules[$locale.'.'.$name] = sprintf($rule, $locale);
           }
       }

       return $rules;
   }
}
