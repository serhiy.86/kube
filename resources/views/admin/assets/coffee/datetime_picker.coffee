window.initDateTimePickers = () ->
  $('.datepicker-birthday').datepicker
    autoclose: true
    language: window.lang
    todayHighlight: true
    format: birthday_format
    todayBtn: true

  $('.datepicker').datepicker
    autoclose: true
    language: window.lang
    todayHighlight: true
    format: 'yyyy-mm-dd'
    todayBtn: false
    startDate: new Date()

$(document).on "ready", () ->
  initDateTimePickers()