<?php

namespace App\Http\Controllers\Backend\Catalog;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Backend\BackendController;
use App\Http\Requests\Backend\Catalog\ProductCreateRequest;
use App\Http\Requests\Backend\Catalog\ProductUpdateRequest;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use Illuminate\Contracts\Routing\ResponseFactory;
use Yajra\DataTables\Html\Builder;
use Illuminate\Http\Request;

use App\Classes\ImageUploader;
use App\Models\Category;
use App\Models\Product;

use DB;
use Event;
use Exception;
use DataTables;
use Redirect;

/**
 * Class ProductController
 * @package App\Http\Controllers\Backend
 */
class ProductController extends BackendController
{

    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "catalog.product";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'catalog.product.read',
        'create'          => 'catalog.product.create',
        'store'           => 'catalog.product.create',
        'show'            => 'catalog.product.read',
        'edit'            => 'catalog.product.read',
        'update'          => 'catalog.product.write',
        'destroy'         => 'catalog.product.delete',
        'ajaxFieldChange' => 'catalog.product.write',
    ];

    /**
     * Display a listing of the resource.
     * GET /Product
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Product::withTranslations()->joinTranslations('products', 'products_translations')->select(
                    'products.id',
                    'products_translations.title',
                    'products.created_at',
                    'products.created_by',
                    'products.modified_by',
                    'status'
                );
            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'title',
                    function ($query, $keyword) {
                        $query->whereRaw('product_translations.title like ?', ['%{$keyword}%']);
                    }
                )
                ->editColumn(
                    'created_by',
                    function ($model) {
                        return $model->creator ? $model->creator->email : '';
                    }
                )
                ->editColumn(
                    'modified_by',
                    function ($model) {
                        return $model->moderator ? $model->moderator->email : '';
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->editColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'actions'])
                ->make();

        }

        $this->data('page_title', trans('labels.product'));
        $this->breadcrumbs(trans('labels.list'));
        return $this->render('views.'.$this->module.'.index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /product/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new Product());

        $this->data('page_title', trans('labels.creating'));

        $this->breadcrumbs(trans('labels.product'));

        $this->_fillAdditionTemplateData();
      //  dd('views.'.$this->module.'.create');

        return $this->render('views.'.$this->module.'.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /Product
     *
     * @param ProductCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductCreateRequest $request)
    {
        DB::beginTransaction();

        try {

            $input = $request->all();

            $input['author_id'] = 0;
            $input['speaker_id'] = 0;
            $categories = $request->get('product_categories');
            $input['status'] = (int)$input['status'];
            $input['for_authorized'] = (int)$input['for_authorized'];
            $input['price'] = (int)$input['price'];
            $input['catalog_filter_id'] = 0;
            $input['created_by'] = $this->user->id;
            $model = new Product($input);
            //dd($model);
            $model->fill($input);


            $model->save();
            if($categories) {
                $model->categories()->sync($categories);
            }

            DB::commit();

//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add('error', trans('messages.save_failed'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * GET /product/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /product/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        try {
            $model = Product::findOrFail($id);

            $this->data('page_title', trans('labels.editing'));

            $this->breadcrumbs(trans('labels.product'));

            $this->_fillAdditionTemplateData();

            return $this->render('views.'.$this->module.'.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /product/{id}
     *
     * @param  int           $id
     * @param ProductCreateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, ProductUpdateRequest $request)
    {
        try {
            $input = $request->all();

            $categories = $request->get('product_categories');
            $model = Product::findOrFail($id);

            $model->fill($input);
            $model->status = $input['status'];
            $model->modified_by = $this->user->id;
            $model->save();
            if($categories){

                $model->categories()->sync($categories);
            }  else {
                $model->categories()->sync(array());
            }

//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
          } catch (Exception $e) {
              DB::rollBack();

  //            FlashMessages::add("error", trans('messages.update_error'));

              return redirect()->back()->withInput();
          }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /product/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $model = Product::findOrFail($id);

            if (!$model->delete()) {
//                FlashMessages::add("error", trans("messages.destroy_error"));
            } else {
//                FlashMessages::add('success', trans("messages.destroy_ok"));
            }
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
//            FlashMessages::add("error", trans('messages.delete_error'));
        }

        return redirect()->route('admin.'.$this->module.'.index');
    }

    private function _fillAdditionTemplateData()
    {
        $categories = Category::withTranslations()->joinTranslations('categories', 'category_translations', 'id', 'category_id')->pluck('category_translations.name', 'categories.id')->toArray();
        $this->data('categories', $categories);
    }
}
