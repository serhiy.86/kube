<?php

namespace App\Http\Requests\Backend\Role;

use App\Http\Requests\BaseRequest;

/**
 * Class RoleUpdateRequest
 * @package App\Http\Requests\Role
 */
class RoleUpdateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('role');
        
        return [
            'name' => 'required|min:2|unique:roles,name,'.$id,
        ];
    }
}