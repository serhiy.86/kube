<?php

namespace App\Http\Requests\Backend\Page;

use App\Http\Requests\BaseRequest;
use App\Rules\MaxStrLen;
use Config;

/**
 * Class PageCreateRequest
 * @package App\Http\Requests\Backend\Page
 */
class PageCreateRequest extends BaseRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //$templates = get_templates(base_path('resources/themes/'.config('app.theme').'/views/page/templates'), true);
        
        $rules = [
            'status'   => 'required|boolean',
            'slug'     => 'unique:pages,slug',
            'position' => 'required|integer',
//            'template' => 'required|in:'.implode(',', $templates),
//            'image'    => $this->defaultRules['image'],
        ];
        
        $languageRules = [
            'name' => 'required',
            'quote' => 'string|nullable|max:520',
        ];
        
        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = $rule;
            }
        }
        
        return $rules;
    }
}
