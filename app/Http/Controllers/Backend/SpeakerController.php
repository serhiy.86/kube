<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Speaker\SpeakerCreateRequest;
use App\Http\Requests\Backend\Speaker\SpeakerUpdateRequest;
use App\Models\Speaker;
use App\Http\Controllers\Traits\AjaxFieldsChangerTrait;
use App\Http\Controllers\Traits\SaveImageTrait;
use DataTables;
use DB;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class SpeakerController
 * @package App\Http\Controllers\Backend
 */
class SpeakerController extends BackendController
{

    use SaveImageTrait;
    use AjaxFieldsChangerTrait;

    /**
     * @var string
     */
    public $module = "speaker";

    /**
     * @var array
     */
    public $accessMap = [
        'index'           => 'speaker.read',
        'create'          => 'speaker.create',
        'store'           => 'speaker.create',
        'show'            => 'speaker.read',
        'edit'            => 'speaker.read',
        'update'          => 'speaker.write',
        'destroy'         => 'speaker.delete',
        'ajaxFieldChange' => 'speaker.write',
    ];

    /**
     * Display a listing of the resource.
     * GET /Speaker
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        if ($request->get('draw')) {
            $list = Speaker::withTranslations()->joinTranslations('speakers', 'speaker_translations')->select(
                'speakers.id',
                'speakers.name',
                'speakers.posada',
                'status'
            );

            return $dataTables = DataTables::of($list)
                ->filterColumn(
                    'name',
                    function ($query, $keyword) {
                        $query->whereRaw("speakers.name like ?", ["%{$keyword}%"]);
                    }
                )
                ->filterColumn(
                    'posada',
                    function ($query, $keyword) {
                        $query->whereRaw("speakers.posada like ?", ["%{$keyword}%"]);
                    }
                )
                ->editColumn(
                    'status',
                    function ($model) {
                        return view(
                            'partials.datatables.toggler',
                            ['model' => $model, 'type' => $this->module, 'field' => 'status']
                        )->render();
                    }
                )
                ->addColumn(
                    'actions',
                    function ($model) {
                        return view(
                            'partials.datatables.control_buttons',
                            ['model' => $model, 'type' => $this->module]
                        )->render();
                    }
                )
                ->rawColumns(['status', 'actions'])
                ->make();
        }

        $this->data('page_title', trans('labels.speakers'));
        $this->breadcrumbs(trans('labels.list'));

        return $this->render('views.'.$this->module.'.index');
    }

    /**
     * Show the form for creating a new resource.
     * GET /speaker/create
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->data('model', new Speaker());

        $this->data('page_title', trans('labels.creating'));

        $this->breadcrumbs(trans('labels.creating'));

        return $this->render('views.'.$this->module.'.create');
    }

    /**
     * Store a newly created resource in storage.
     * POST /Speaker
     *
     * @param SpeakerRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SpeakerCreateRequest $request)
    {


        DB::beginTransaction();

        try {

            $input = $request->all();

            $model = new Speaker($input);
            $model->status = $input['status'];
            $model->save();
            $this->setImage($model, 'avatar', $this->module);
            DB::commit();

//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add('error', trans('messages.save_failed'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     * GET /speaker/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     * GET /speaker/{id}/edit
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        try {
            $model = Speaker::findOrFail($id);

            $this->data('page_title', trans('labels.editing'));

            $this->breadcrumbs(trans('labels.editing'));

            return $this->render('views.'.$this->module.'.edit', compact('model'));
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /speaker/{id}
     *
     * @param  int           $id
     * @param SpeakerRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, SpeakerUpdateRequest $request)
    {

        try {
            $input = $request->all();
            $model = Speaker::findOrFail($id);

            $model->fill($input);
            $model->status = $input['status'];

            $model->save();
            $this->setImage($model, 'avatar', $this->module);
//            FlashMessages::add('success', trans('messages.save_ok'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));

            return redirect()->route('admin.'.$this->module.'.index');
        } catch (Exception $e) {
            DB::rollBack();

//            FlashMessages::add("error", trans('messages.update_error'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /speaker/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $model = Speaker::findOrFail($id);

            if (!$model->delete()) {
//                FlashMessages::add("error", trans("messages.destroy_error"));
            } else {
//                FlashMessages::add('success', trans("messages.destroy_ok"));
            }
        } catch (ModelNotFoundException $e) {
//            FlashMessages::add('error', trans('messages.record_not_found'));
        } catch (Exception $e) {
//            FlashMessages::add("error", trans('messages.delete_error'));
        }

        return redirect()->route('admin.'.$this->module.'.index');
    }

    private function __processImages($model)
    {
        $images = request('images.remove', []);
        foreach ($images as $image) {
            try {
                $_image = $model->images()->find($image);
                $_image->delete();
            } catch (Exception $e) {
//                FlashMessages::add('error', $e->getMessage());
                continue;
            }
        }


        $images = request('images.old', []);
        foreach ($images as $key => $image) {
            try {
                $_image = $model->images()->findOrFail($key);
                $_image->update($image);
            } catch (Exception $e) {
//                FlashMessages::add('error', $e->getMessage());
                continue;
            }
        }

        $images = request('images.new', []);
        foreach ($images as $key => $image) {
            try {
                $_image = new Image();
                $_image->fill($image);
                $model->images()->save($_image);

            } catch (Exception $e) {
//                FlashMessages::add('error', $e->getMessage());
                continue;
            }
        }
        return true;
    }

}
