<?php

namespace App\Services;

use App\Contracts\UserServiceContract;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

/**
* Class UserService
* @package App\Services
*/
class UserService
{
    /**
     * @param array $data
     * @param string $role
     * @param bool $activate
     *
     * @return bool|\Cartalyst\Sentinel\Users\UserInterface|User
     * @throws \Exception
     */
    public function create(array $data, string $role = 'user', $activate = false)
    {
//        if (!isset($data['uuid'])) {
//            $data['uuid'] = Uuid::generate();
//        }

        if ($activate) {
            $user = Sentinel::registerAndActivate($data);
        } else {
            $user = Sentinel::register($data);
        }

        $user->info()->create($data);
        $locale = request()->header('X-Localization', config('app.locale'));
        $user->setting()->create(['locale' => $locale]);

        $role = Sentinel::getRoleRepository()->findBySlug($role);
        if($role) $role->users()->attach($user);

        return $user;
    }

    public function add_role(User $user, string $role = 'user')
    {

        $role = Sentinel::getRoleRepository()->findBySlug($role);
        $role->users()->attach($user);

        return $user;
    }

    private function with_rel(User $user, $name)
    {
        $user->$name()->create([]);
        return true;
    }

    public function update(User $user, array $data)
    {
		$user = Sentinel::update($user, $data);

		try{

			$user->info()->first()->fill($data)->save();

		} catch (\Exception $e) {

			\Log::error('update info',$data);

			return NULL;
		}
        return $user;
    }

    /**
     * @param array $data
     *
     * @return \App\Models\User
     */
    public function findOrCreateActivatedUser(array $data)
    {
        $user = Sentinel::findByCredentials(['email' => $data['email']]);

        if (!$user) {
            $user = $this->create($data, true);
        }

        return $user;
    }


    /**
     * @param \App\Models\User $user
     *
     * @return \App\Models\User $user
     */
    public function activate(User $user)
    {
        if (Activation::completed($user)) {
            return $user;
        }

        $activation = Activation::exists($user);

        if (!$activation) {
            $activation = Activation::create($user);
        } else {
            $activation = Activation::get($user);
        }

        Activation::complete($user, $activation->getCode());

        return $user;
    }

    public function completed(User $user) {

		return Activation::completed($user);

    }

    public function activation(User $user,$code)
    {
        if (Activation::completed($user)) {
            return 2;
        }

        if (Activation::complete($user, $code)) {
			return 1;
		}

        return 3;
    }


    /**
     * @param \App\Models\User $user
     *
     * @return \App\Models\User $user
     */
    public function deactivate(User $user)
    {
        $activation = Activation::completed($user);

        if ($activation) {

            try{

	            Activation::remove($user);

            } catch(\Exception $e) {
	            \Log::error('Deactivate',[$e->getMessage()]);
            }
        }

        return $user;
    }

    /**
     * @param \App\Models\User $user
     *
     * @return boolean
     */
    public function delete(User $user)
    {
        $error = false;

        if(!$error) {
	        try {

	            $user->delete();

	        } catch (\Exception $e) {
	            return $e->getMessage();
	        }
        }

        return ($error) ? $error : NULL;
    }

	public function getRepository()
	{
		return Sentinel::getUserRepository();
	}

	public function checkPassword($oldPassword, $password)
	{
		$hasher = Sentinel::getHasher();

		if (!$hasher->check($oldPassword, $password)) {
                    return false;
                }

		return true;
	}

}
