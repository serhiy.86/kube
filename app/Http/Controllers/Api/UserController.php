<?php namespace App\Http\Controllers\Api;

use App\Events\ChangePassword;
use App\Events\ChangeUserData;
use App\Http\Requests\User\PasswordChangeRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserPhotoUpdateRequest;

use App\Api\Transformers\UserTransformer;

use App\Models\User;
use App\Services\UserService;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ApiController;

class UserController extends ApiController
{
    /**
     * Eloquent model.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function model()
    {
        return new User;
    }

    /**
     * Transformer for the current model.
     *
     * @return \League\Fractal\TransformerAbstract
     */
    protected function transformer()
    {
        return new UserTransformer;
    }

    public function changePassword(PasswordChangeRequest $request,UserService $user_service)
    {
        //$oldPassword = $request->get('old_password');
        $password = $request->get('password');
        $passwordConf = $request->get('password_confirmation');

        $user = $this->auth->getUser();

        //if (!$user_service->checkPassword($oldPassword, $user->password) || $password != $passwordConf) {
        //    return $this->respondWithError('invalid_password', 422);
        //}

        $user_service->update($user, array('password' => $password));

        event(new ChangePassword($user));

        return response()->json(['data'=> ['success'=> true, 'message' => trans('validation.password_successfully_changed')] ]);
    }

    public function me()
    {
		$user = $this->auth->getUser();

        return $this->respondWithItem($user);
    }

    /**
     * @param UserApiUpdateRequest $request
     * @return mixed
     */
    public function updateMyInfo(UserUpdateRequest $request,UserService $user_service)
    {
        $user = $this->auth->getUser();

        $data = $request->only('first_name', 'last_name', 'email');

        if( $user_service->update($user, $data) ){

	        event(new ChangeUserData($user));

		}

        return $this->respondWithItem($user);
    }

    /**
     * @param UserPhotoUpdateRequest $request
     * @return mixed
     */
    public function updateMyPhoto(UserPhotoUpdateRequest $request,UserService $user_service)
    {
        $user = $this->auth->getUser();

        $path = $request->file('photo')->store('avatars','public');

		$photo_url = Storage::disk('public')->url($path);

        if( $user_service->update($user, ['avatar'=>$photo_url]) ){

	        event(new ChangeUserData($user));

		}

        return $this->respondWithItem($user);
    }
  
	public function delete(UserService $user_service)
	{
		$user = $this->auth->getUser();

		$msg = $user_service->delete($user); // msg = error occured
		
		if($msg) {

			return $this->respondWithError($msg, 422);
		}

		return $this->respondWithArray([
			'status' => ($msg) ? false : true,
			'message' => $msg,
		]);
	}

}
