<?php

return array (
    'lengthMenu'   => '_MENU_ records per page',
    'zeroRecords'  => 'No Records Found',
    'info'         => 'Displayed from _START_ to _END_ records with _TOTAL_',
    'search'       => 'Search: ',
    'infoEmpty'    => 'No records found',
    'infoFiltered' => '- filtered from _MAX_ records',
    'paginate'     => [
        'first'    => 'To the begining',
        'last'     => 'In the end',
        'next'     => 'Next',
        'previous' => 'Previous',
    ],
);
