<?php

namespace App\Http\Requests\Backend\Blog;

use App\Http\Requests\BaseRequest;

/**
 * Class BlogUpdateRequest
 * @package App\Http\Requests\Blog
 */
class BlogUpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('blog');
        $rules = [
            'status' => 'required|boolean',
            'slug' => 'unique:blog,slug,'.$id.',id',
        ];

        $languageRules = [
            'title' => 'required|string',
            'text' => 'required|string',
            'short_description' => 'nullable|string',
            'meta_title' => 'nullable|string',
            'meta_description' => 'nullable|string',
            'meta_keyword' => 'nullable|string',
        ];

        foreach (config('app.locales') as $locale) {
            foreach ($languageRules as $name => $rule) {
                $rules[$locale.'.'.$name] = sprintf($rule, $locale);
            }
        }

        return $rules;
    }
}
