<?php

namespace App\Http\Requests\User;

use App\Models\User;
use App\Http\Requests\BaseRequest;

/**
 * Class UserUpdateRequest
 * @package App\Http\Requests\User
 */
class UserPhotoUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        return $rules;
    }
}
