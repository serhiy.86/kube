@extends('admin.layouts.editable')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            {!! Form::model($model, array('role' => 'form', 'method' => 'put', 'route' => array('admin.role.update', $model->id))) !!}

                @include('views.role.partials._form')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
