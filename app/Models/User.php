<?php

namespace App\Models;

use Activation;
use Illuminate\Notifications\Notifiable;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;
use Joshwhatk\Cent\AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Auth\Authenticatable;
use Webpatser\Uuid\Uuid;

class User extends SentinelUser implements AuthenticatableContract,JWTSubject
{
    use Notifiable, Authenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'reg_complete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'permissions' => 'array',
    ];

	protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
			$model->uuid = Uuid::generate();
        });
    }

    /**
     * Dynamically pass missing methods to the user.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $methods = ['hasAccess'];

        if (in_array($method, $methods)) {
            if (isset($this->permissions['superuser']) && $this->permissions['superuser'] == true) {
                return true;
            }
        }

        return parent::__call($method, $parameters);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

	/**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
	public function social()
	{
		return $this->hasMany(UserSocial::class);
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function setting()
    {
        return $this->hasOne(UserSetting::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @param $q
     */
    public function scopeJoinInfo($q)
    {
        return $q->leftJoin('user_info', 'users.id', '=', 'user_info.user_id');
    }

    /**
     * @param mixed $value
     *
     * @return null|string
     */
    public function getActivatedAttribute($value)
    {
        return (bool) Activation::completed($this);
    }

    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        if (empty($this->info->avatar)) {
            return config('user.no_image');
        }

        return $this->info->avatar;
    }

    /**
     * @return string
     */
    public function getFirstNameAttribute()
    {
        if (empty($this->info->first_name)) {
            return null;
        }

        return $this->info->first_name;
    }

    /**
     * @return string
     */
    public function getLastNameAttribute()
    {
        if (empty($this->info->last_name)) {
            return null;
        }

        return $this->info->last_name;
    }

    /**
     * @return string
     */
    public function getGenderAttribute()
    {
        if (empty($this->info->gender)) {
            return null;
        }

        return $this->info->gender;
    }


    /**
     * @return string
     */
    public function getPhoneAttribute()
    {
        if (empty($this->info->phone)) {
            return '';
        }

        return $this->info->phone;
    }

    /**
     * @return array
     */
    public function getGenders(): array
    {
        return $this->genders;
    }

    /**
     * @param int $id
     *
     * @return string|null
     */
    public function getGenderKeyById(int $id)
    {
        foreach ($this->genders as $_id => $key) {
            if ($_id != $id) {
                continue;
            }

            return $key;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        if (empty($this->info->last_name)) {
            return null;
        } elseif (empty($this->info->first_name)) {
            return null;
        }
        return trim($this->info->last_name . ' ' . $this->info->first_name);
    }

    /**
     * @return string
     */
    public function getStringGender(): string
    {
        if (empty($this->info->gender)) {
            return '';
        }

        return $this->getGenderKeyById($this->info->gender);
    }

}
